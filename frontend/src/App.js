import React, { createContext, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";
import LoadRouteComponent from "./components/load-router/load-route.component";
import getUserInfo from "./utils/getUserInfo";
import isAuthenticated from "./utils/isAuthenticated";
import NotFound from "./components/exceptions/notFound/NotFound";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import routes from "./routes";

import { GlobalStyle } from "./components/styles/globalStyle";
import "./assets/fonts/theme-icons/style.css";
import "./assets/images/css-sprite/exchange-object-icons.css";

export const UserContext = createContext({});

const App = () => {

  const [authentication, setAuthentication] = useState(isAuthenticated());

  return (
    <Router>
      <UserContext.Provider value={getUserInfo()}>
        <HelmetProvider>
          <Header authentication={authentication} setAuthentication={setAuthentication} />
          <Switch>
            {routes.map((route, i) => (
              <LoadRouteComponent key={i} {...route} />
            ))}
            <Route path="*" render={() => <NotFound />} />
          </Switch>
          <Footer/>
        </HelmetProvider>
      </UserContext.Provider>
      <GlobalStyle />
    </Router>
  );
};

export default App;
