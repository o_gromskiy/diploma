import Cookies from 'js-cookie';
import jwt_decode from "jwt-decode";

const getUserInfo = () => {
  let token = null;
  if (Cookies.get('token') !== '') {
    try {
      token = jwt_decode(Cookies.get('token'));
    } catch (e) {
      return null;
    }
    return token;
  }
  return null;
};

export default getUserInfo;
