import React from 'react';
import Moment from 'react-moment';

const DateConverter = (str) => {
  return <Moment format="DD-MM-YYYY HH:mm:ss">{str}</Moment>;
};
export default DateConverter;
