import Cookies from 'js-cookie';

const authenticationConfig = () => {
  if (Cookies.get('token') !== '') {
    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + Cookies.get('token')
      }
    };
  }
  return {
    headers: {
      'Content-Type': 'application/json'
    }
  };
};

export default authenticationConfig;
