import React, { useContext } from "react";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import Can from "../../components/can/Can";
import { clients } from "../../rbac-consts";
import ClientsList from "../../components/clients/ClientsList";

const Clients = () => {
  const { role } = useContext(UserContext);

  return (
    <Can
      role={role}
      perform={clients.READ}
      yes={() => (
        <ClientsList />
      )}
      no={() => <Forbidden />}
    />
  );
};

export default Clients;
