import React from "react";
import { Helmet } from "react-helmet-async";
import Title from "../../components/title/Title";
import CalculatorContainer from "../../components/calculator/CalculatorContainer";
import ServiceSection from "../../components/home/services/ServiceSection";
import CounterSection from "../../components/home/num-count/CounterSection";
import PrivilegeSection from "../../components/home/privilege/PrivilegeSection";
import SolutionSection from "../../components/home/solution/SolutionSection";
import ReviewsSection from "../../components/home/reviews/ReviewsSection";
import RatesChartSection from "../../components/home/ratesChart/RatesChartSection";

import { StyledContainer } from "../../components/styles/styledContainer";
import { StyledHomeWrapper } from "./styledHome";

const Home = () => {

  return (
    <StyledHomeWrapper>
      <Helmet>
        <title>Cryptocurrency exchange - X-Changer.ml</title>
        <meta
          name="description"
          content="Buy and sell cryptocurrencies online 💳"
        />
      </Helmet>
      <StyledContainer wrapper="content">
        <Title value="Cryptocurrency exchange" />
        <CalculatorContainer />
      </StyledContainer>
      <RatesChartSection />
      <ServiceSection />
      <CounterSection />
      <PrivilegeSection />
      <SolutionSection />
      <ReviewsSection />
    </StyledHomeWrapper>
  );

};

export default Home;