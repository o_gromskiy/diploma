import React, { useContext } from "react";
import RequisitionsList from "../../components/requisitions/RequisitionsList";
import Can from "../../components/can/Can";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import { requisition } from "../../rbac-consts";

const Requisitions = () => {

  const { role } = useContext(UserContext);

  return (
    <Can
      role={role}
      perform={requisition.READ}
      yes={() => (
        <RequisitionsList />
      )}
      no={() => (<Forbidden />)}
    >
    </Can>
  );
};

export default Requisitions;
