import React, { useContext } from "react";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import Can from "../../components/can/Can";
import { currency } from "../../rbac-consts";
import CurrencyList from "../../components/currency/CurrencyList";

const Currency = () => {
  const { role } = useContext(UserContext);

  return (
    <Can
      role={role}
      perform={currency.READ}
      yes={() => (
        <CurrencyList />
      )}
      no={() => <Forbidden />}
    />
  );
};

export default Currency;
