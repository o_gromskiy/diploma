import styled from "styled-components";

export const StyledRegistrationWrapper = styled.div`
  padding: 125px 0;
  @media screen and (max-width: 992px) {
    padding: 50px 0;
  }
  @media screen and (max-width: 576px) {
    padding: 25px 0;
  }
`;
export const StyledRegistrationContent = styled.div`
  max-width: 460px;
  width: 100%;
  margin: 0 auto;
  padding: 20px 15px;
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
`;

export const StyledRegistrationForm = styled.form`
  display: grid;
  grid-template-columns: 100%;
  grid-gap: 15px;

  .registration-form__title {
    margin: 0 auto;
    padding: 20px 0;
    color: #008D75;
    font-size: 22px;
    font-weight: 700;
    @media screen and (max-width: 768px) {
      padding: 10px 0;
    }
  }

  .registration-form__action {
    padding-top: 15px;
    display: flex;
    justify-content: center;
  }

  .registration-form__link {
    padding: 20px 0;
    color: #008D75;
    opacity: 0.75;
    text-align: center;
  }
`;