import React, { useContext } from "react";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import Can from "../../components/can/Can";
import { paymentSystem } from "../../rbac-consts";
import PaymentSystemContainer from "../../components/paymentSystem/PaymentSystemContainer";

const PaymentSystem = () => {
  const { role } = useContext(UserContext);

  return (
    <Can
      role={role}
      perform={paymentSystem.READ}
      yes={() => (
        <PaymentSystemContainer />
      )}
      no={() => <Forbidden />}
    />
  );
};

export default PaymentSystem;
