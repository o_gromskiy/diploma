import React, { useEffect, useState } from "react";
import { getExpirationDate, isExpired } from "../../utils/checkExpiredToken";
import Cookies from "js-cookie";
import nprogress from "nprogress";
import axios from "axios";
import Notification from "../../components/notification/notification";
import { GoogleLogin } from "react-google-login";

import { StyledLoginContent, StyledLoginForm, StyledLoginWrapper, StyledSocialLogin } from "./styledLogin";
import { StyledContainer } from "../../components/styles/styledContainer";
import { CustomTextfield } from "../../components/textfield/CustomTextfield";
import MyButton from "../../components/styles/materialButtonStyle";
import { Helmet } from "react-helmet-async";

const Login = () => {

  const [visible, setVisible] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
    message: ""
  });

  const handleChangeAlert = (message, type) => {
    setVisible({ ...visible, open: true, type: type, message: message });
  };

  const handleCloseAlert = () => {
    setVisible({ ...visible, open: false });
  };

  useEffect(() => {
    if (isExpired(getExpirationDate(Cookies.get("token")))) {
      Cookies.remove("token");
    }
    nprogress.done();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    let data = {
      username: event.target.email.value,
      password: event.target.password.value
    };

    axios.post(`/api/login_check`, data).then(response => {
      if (response.status === 200) {
        Cookies.set("token", response.data.token);
        window.location.href = "/panel/requisition";
      }
    }).catch(error => {
      if (error.response.status === 401) {
        handleChangeAlert("Invalid login or password", "error");
      }
      if (error.response.status === 403) {
        handleChangeAlert(error.response.data.detail, "error");
      }
    });
  };

  const responseGoogle = (response) => {
    let data = {
      token: response.tokenId
    };

    axios.post("/api/google_check", data).then(response => {
      if (response.status === 200) {
        Cookies.set("token", response.data.token);
        window.location.href = "/panel/requisition";
      }
    }).catch(error => {
      if (error.response.status === 403) {
        handleChangeAlert(error.response.data.detail, "error");
      }
    });
  };

  return (
    <StyledContainer>
      <Helmet>
        <title>Login - X-Changer.ml</title>
      </Helmet>
      <Notification
        type={visible.type}
        visible={visible}
        close={handleCloseAlert}
        message={visible.message}
      />
      <StyledLoginWrapper>
        <StyledLoginContent>
          <StyledLoginForm onSubmit={handleSubmit}>
            <h1 className="login-form__title">Login to your personal account:</h1>
            <CustomTextfield
              id="email"
              name="email"
              autoComplete="off"
              required
              label="E-mail"
            />
            <CustomTextfield
              id="password"
              type="password"
              name="password"
              autoComplete="off"
              required
              label="Password"
            />
            <div className="login-form__action">
              <MyButton color="success" aria-haspopup="true" type="submit">
                Login
              </MyButton>
            </div>
          </StyledLoginForm>
          <StyledSocialLogin>
            <p>
              or use social networks
            </p>
            <GoogleLogin
              clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
              buttonText="Login"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={"single_host_origin"}
              className="login-form__button"
            />
          </StyledSocialLogin>
        </StyledLoginContent>
      </StyledLoginWrapper>
    </StyledContainer>
  );
};

export default Login;
