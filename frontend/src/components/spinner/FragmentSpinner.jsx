import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import { StyledFragmentSpinner } from './styledSpinner';

const FragmentSpinner = ({position}) => {
  return (
    <StyledFragmentSpinner position={position}>
      <CircularProgress/>
    </StyledFragmentSpinner>
  );
};

export default FragmentSpinner;