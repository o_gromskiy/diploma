import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

import { StyledDefaultSpinner } from "./styledSpinner";

const Spinner = () => {
  return (
    <StyledDefaultSpinner className="default-spinner">
      <CircularProgress color="inherit" />
    </StyledDefaultSpinner>
  );
};

export default Spinner;