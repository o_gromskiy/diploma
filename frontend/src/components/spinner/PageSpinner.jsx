import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

import { StyledDefaultSpinner } from "./styledSpinner";
import { StyledContainer } from "../styles/styledContainer";

const PageSpinner = () => {
  return (
    <StyledContainer>
      <StyledDefaultSpinner>
        <CircularProgress color="inherit" />
      </StyledDefaultSpinner>
    </StyledContainer>

  );
};

export default PageSpinner;