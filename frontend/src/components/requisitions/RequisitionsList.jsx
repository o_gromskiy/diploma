import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import { NavLink, useHistory } from "react-router-dom";
import authenticationConfig from "../../utils/authenticationConfig";
import Alert from "@material-ui/lab/Alert";
import Can from "../can/Can";
import { requisition as requisitionRule } from "../../rbac-consts";
import { UserContext } from "../../App";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import PageSpinner from "../spinner/PageSpinner";

import { StyledExchange, StyledRequisitionsList } from "./styledRequisitions";
import { StyledContainer } from "../styles/styledContainer";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";
import MyButton from "../styles/materialButtonStyle";
import DateConverter from "../../utils/DateConverter";
import Title from "../title/Title";
import RenderStatus from "../styles/styledStatus";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import MaterialPagination from "../styles/materialPagination";
import { Helmet } from "react-helmet-async";

const RequisitionsList = () => {

  const { role } = useContext(UserContext);
  const [requisitions, setRequisitions] = useState({});
  const [fetching, setFetching] = useState(true);
  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchRequisitions = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/requisitions" + filterUrl, authenticationConfig()).then(response => {
          setRequisitions(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchRequisitions();
  }, [filterData]);

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  if (fetching) {
    return <PageSpinner />;
  }

  return (
    <StyledContainer>
      <Helmet>
        <title>Requisitions list - X-Changer.ml</title>
      </Helmet>
      <StyledRequisitionsList role={role}>
        <Title value="Список обменов" />
        {(requisitions.length === 0) ?
          <Alert severity="warning">Requisitions are empty</Alert> :
          <StyledTable className="requisitions-table">
            <StyledTableHead className="requisitions-table__head">
              <StyledColHead>
                Number
              </StyledColHead>
              <Can
                role={role}
                perform={requisitionRule.CLIENTS}
                yes={() => (
                  <StyledColHead>
                    Client
                  </StyledColHead>
                )}
              >
              </Can>
              <StyledColHead>
                Exchange
              </StyledColHead>
              <StyledColHead>
                Status
              </StyledColHead>
              <StyledColHead>
                Created at
              </StyledColHead>
              <StyledColHead />
            </StyledTableHead>
            <StyledTableBody>
              {requisitions.map((requisition, key) => (
                <StyledRow
                  key={key}
                  className="requisitions-table__row"
                >
                  <StyledCol data-title="Number">
                    {requisition.id}
                  </StyledCol>
                  <Can
                    role={role}
                    perform={requisitionRule.CLIENTS}
                    yes={() => (
                      <StyledCol data-title="Client">
                        {requisition.client.email}
                      </StyledCol>
                    )}
                  >
                  </Can>
                  <StyledCol data-title="Exchange">
                    <StyledExchange>
                      <div className="payment exchange-object">
                        {requisition.payment} {requisition.exchangePair.payment.exchangeObject.asset}
                      </div>
                      <div className="icon-exchange" />
                      <div className="payout exchange-object">
                        {requisition.payout} {requisition.exchangePair.payout.exchangeObject.asset}
                      </div>
                    </StyledExchange>

                  </StyledCol>
                  <StyledCol
                    inline
                    data-title="Status"
                  >
                    {/*{requisition.status}*/}
                    <RenderStatus status={requisition.status} />
                  </StyledCol>
                  <StyledCol data-title="Created at">
                    {DateConverter(requisition.createdAt)}
                  </StyledCol>
                  <StyledCol data-title="Action">
                    <div className="requisitions-table__action">
                      <MyButton
                        component={NavLink}
                        to={`/panel/requisition/details/${requisition.id}`}
                        size="small"
                        color="success"
                      >
                        Details
                      </MyButton>
                    </div>
                  </StyledCol>
                </StyledRow>
              ))}
            </StyledTableBody>
          </StyledTable>
        }
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledRequisitionsList>
    </StyledContainer>
  );
};

export default RequisitionsList;
