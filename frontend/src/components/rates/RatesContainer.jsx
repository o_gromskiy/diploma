import React, { useEffect, useState } from "react";
import axios from "axios";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import PageSpinner from "../spinner/PageSpinner";
import Title from "../title/Title";
import { Helmet } from "react-helmet-async";

import { StyledRatesWrapper } from "./styledRates";
import { StyledContainer } from "../styles/styledContainer";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";


const RatesContainer = () => {

  const [pairs, setPairs] = useState({});
  const [fetching, setFetching] = useState(true);

  const fetchRates = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get("/api/exchange_pairs?active=true").then(response => {
          setPairs(response.data);
          setFetching(false);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    let url = new URL(process.env.REACT_APP_MERCURE_URL);
    url.searchParams.append("topic", `create:pair`);

    const eventSource = new EventSource(url);

    eventSource.onmessage = (event) => {
      fetchRates();
    };

    fetchRates();

    return () => {
      eventSource.close();
    };
  }, []);

  if (fetching) {
    return <PageSpinner />;
  }

  const renderItems = (pair) => {
    if (pair.payment.exchangeObject.type === "currency") {
      return (
        <React.Fragment>
          <StyledCol data-title="You give">
            <div className="currency-wrapper">
              <div className={`currency-wrapper__icon exchange-icon-${pair.payment.paymentSystem.code}`} />
              <div className="currency-wrapper__name">
                {pair.payment.exchangeObject.asset} {pair.payment.paymentSystem.name}
              </div>
            </div>
          </StyledCol>
          <StyledCol data-title={`You give ${pair.payment.exchangeObject.asset}`}>
            <b>1</b>
          </StyledCol>
          <StyledCol data-title="You get">
            <div className="currency-wrapper">
              <div className={`currency-wrapper__icon exchange-icon-${pair.payout.exchangeObject.asset}`} />
              <div className="currency-wrapper__name">
                {pair.payout.exchangeObject.asset}
              </div>
            </div>
          </StyledCol>
          <StyledCol data-title={`You get ${pair.payout.exchangeObject.asset}`}>
            <b>{1 / (pair.payment.exchangeObject.course * pair.payout.exchangeObject.course)}</b>
          </StyledCol>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <StyledCol data-title="You give">
            <div className="currency-wrapper">
              <div className={`currency-wrapper__icon exchange-icon-${pair.payment.exchangeObject.asset}`} />
              <div className="currency-wrapper__name">
                {pair.payment.exchangeObject.asset}
              </div>
            </div>
          </StyledCol>
          <StyledCol data-title={`You give ${pair.payment.exchangeObject.asset}`}>
            <b>1</b>
          </StyledCol>
          <StyledCol data-title="You get">
            <div className="currency-wrapper">
              <div className={`currency-wrapper__icon exchange-icon-${pair.payout.paymentSystem.code}`} />
              <div className="currency-wrapper__name">
                {pair.payout.exchangeObject.asset} {pair.payout.paymentSystem.name}
              </div>
            </div>
          </StyledCol>
          <StyledCol data-title={`You get ${pair.payout.exchangeObject.asset}`}>
            <b>{pair.payout.exchangeObject.course * pair.payment.exchangeObject.course}</b>
          </StyledCol>
        </React.Fragment>
      );
    }
  };

  return (
    <StyledContainer>
      <StyledRatesWrapper>
        <Helmet>
          <title>Current rates - X-Changer.ml</title>
          <meta
            name="description"
            content="The best exchange rate for cryptocurrency"
          />
        </Helmet>
        <Title value="Current rates" />
        <StyledTable className="rates-table">
          <StyledTableHead
            col="4"
            className="rates-table__head"
          >
            <StyledColHead>
              You give
            </StyledColHead>
            <StyledColHead />
            <StyledColHead>
             You get
            </StyledColHead>
            <StyledColHead />
          </StyledTableHead>
          <StyledTableBody>
            {pairs && pairs.map((value, key) => (
              <StyledRow
                col="4"
                key={key}
                className="rates-table__row"
              >
                {renderItems(value)}
              </StyledRow>
            ))}
          </StyledTableBody>
        </StyledTable>
      </StyledRatesWrapper>
    </StyledContainer>
  );
};

export default RatesContainer;
