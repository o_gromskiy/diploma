import styled from 'styled-components'

export const StyledLogoWrapper = styled.div`
  display: grid;
  grid-template-columns: 45px max-content;
  align-items: center;
  grid-gap: 10px;
  .logo-title {
    font-size: 26px;
    font-weight: 700;
    background: linear-gradient(326deg, rgba(1,115,95,1) 0%, rgba(44,178,127,1) 77%, rgba(254,209,81,1) 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    color: #008D75;
  }
  img {
    width: 45px;
    height: 45px;
    border-radius: 50%;
    transition: all .3s ease;
  }
  &:hover img {
    transform: scale(0.95) rotate(25deg);
    box-shadow: 0 2px 2px rgba(0, 0, 0, .25);
  }
  @media screen and  (max-width: 576px) {
    grid-template-columns: 45px;
    grid-gap: 0;
    .logo-title {
      display: none;
    }
  }
`;