import React from "react";
import { NavLink } from "react-router-dom";

import logoImage from "../../assets/images/logo.svg";
import { StyledLogoWrapper } from "./styledLogo";

const Logo = () => {
  return (
    <NavLink to="/">
      <StyledLogoWrapper>
        <img src={logoImage} alt="logo" />
        <div className="logo-title">
          X-Changer
        </div>
      </StyledLogoWrapper>
    </NavLink>
  );
};

export default Logo;