import React, { useEffect, useState } from "react";
import axios from "axios";
import Forbidden from "../exceptions/forbidden/Forbidden";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const CryptocurrencySelect = ({ cryptocurrency, setCryptocurrency }) => {

  const [cryptocurrencies, setCryptocurrencies] = useState(null);

  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));

  const classes = useStyles();

  const fetchCryptocurrency = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get("/api/cryptocurrencies").then(response => {
          if (response.status === 200) {
            setCryptocurrencies(response.data["hydra:member"]);
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchCryptocurrency();
  }, []);

  if (Object.keys([cryptocurrencies]).length === 0) {
    return "Empty cryptocurrency";
  }

  const handleChangeSelect = (event) => {
    setCryptocurrency(event.target.value);
  };

  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="cryptocurrency">Cryptocurrency</InputLabel>
      <Select
        labelId="cryptocurrency"
        id="cryptocurrency"
        value={cryptocurrency}
        onChange={handleChangeSelect}
      >
        {cryptocurrencies && cryptocurrencies.map((cryptocurrency, key) => (
            <MenuItem value={cryptocurrency.asset} key={key}>{cryptocurrency.asset}</MenuItem>
          )
        )}
      </Select>
    </FormControl>
  );
};

export default React.memo(CryptocurrencySelect);
