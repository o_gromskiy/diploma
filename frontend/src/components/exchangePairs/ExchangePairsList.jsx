import React, { useState } from "react";
import Alert from "@material-ui/lab/Alert";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Forbidden from "../exceptions/forbidden/Forbidden";
import Spinner from "../spinner/Spinner";

import { StyledChangePercent, StyledExchangePairsList, StyledExchangePrePair } from "./styledExchangePairs";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";
import MaterialSwitch from "../styles/materialSwitch";

const ExchangePairsList = ({ fetching, exchangePairs, setExchangePairs }) => {

  const currency = "Currency";

  const handleActive = (exchangePair, key) => {

    let data = {
      active: !exchangePair.active
    };

    axios.put("/api/exchange_pairs/" + exchangePair.id, data, authenticationConfig()).then(response => {
        setExchangePairs(Object.values({ ...exchangePairs, [key]: response.data }));
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  const handleChangePercent = (id, key, event) => {

    let data = {
      percent: parseFloat(event.target.value)
    };

    axios.put("/api/exchange_pairs/" + id, data, authenticationConfig()).then(response => {
        setExchangePairs(Object.values({ ...exchangePairs, [key]: response.data }));
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  if (fetching) {
    return <Spinner />;
  }



  return (
    <StyledExchangePairsList>
      {(exchangePairs.length === 0) ?
        <Alert severity="warning">Exchange pairs are empty</Alert> :
        <StyledTable className="exchange-pairs-table">
          <StyledTableHead col="5" className="exchange-pairs-table__head">
            <StyledColHead>
              You give
            </StyledColHead>
            <StyledColHead>
              You get
            </StyledColHead>
            <StyledColHead>
              Percent
            </StyledColHead>
            <StyledColHead>
              Status
            </StyledColHead>
            <StyledColHead>
              Action
            </StyledColHead>
          </StyledTableHead>
          <StyledTableBody>
            {exchangePairs.map((exchangePair, key) => (
              <StyledRow col="5" key={key} className="exchange-pairs-table__row">
                <StyledCol data-title=" You give" classaName="exchange-pairs-table__payment">
                  <StyledExchangePrePair>
                    <div className={`pre-pair__icon exchange-icon-${exchangePair.payment.exchangeObject["@type"] === currency ? exchangePair.payment.paymentSystem.code : exchangePair.payment.exchangeObject.asset}`} />
                    <div className="pre-pair__name">
                      {exchangePair.payment.exchangeObject.asset} {exchangePair.payment.exchangeObject["@type"] === currency ? exchangePair.payment.paymentSystem.name : ""}
                    </div>
                  </StyledExchangePrePair>
                </StyledCol>
                <StyledCol data-title=" You get" className="exchange-pairs-table__payout">
                  <StyledExchangePrePair>
                    <div className={`pre-pair__icon exchange-icon-${exchangePair.payout.exchangeObject["@type"] === currency ? exchangePair.payout.paymentSystem.code : exchangePair.payout.exchangeObject.asset}`} />
                    <div className="pre-pair__name">
                      {exchangePair.payout.exchangeObject.asset} {exchangePair.payout.exchangeObject["@type"] === currency ? exchangePair.payout.paymentSystem.name : ""}
                    </div>
                  </StyledExchangePrePair>
                </StyledCol>
                <StyledCol data-title="Percent" className="exchange-pairs-table__percent">
                  <StyledChangePercent>
                    <span>
                      %
                    </span>
                    <input
                      type="text"
                      value={exchangePair.percent}
                      onChange={event => {
                        handleChangePercent(exchangePair.id, key, event);
                      }}
                    />
                  </StyledChangePercent>
                </StyledCol>
                <StyledCol data-title="Status" className="exchange-pairs-table__status">
                  {exchangePair.active ? "Active" : "Not active"}
                </StyledCol>
                <StyledCol data-title="Action" className="exchange-pairs-table__action">
                  <MaterialSwitch
                    checked={exchangePair.active}
                    onChange={() => {handleActive(exchangePair, key);}}
                  />
                </StyledCol>
              </StyledRow>
            ))}
          </StyledTableBody>
        </StyledTable>
      }
    </StyledExchangePairsList>
  );
};

export default ExchangePairsList;
