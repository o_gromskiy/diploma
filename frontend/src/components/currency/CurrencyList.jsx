import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Alert from "@material-ui/lab/Alert";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import { useHistory } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import PageSpinner from "../spinner/PageSpinner";
import Title from "../title/Title";
import MaterialPagination from "../styles/materialPagination";

import { StyledCurrencyWrapper } from "../styles/styledCurrency";
import { StyledContainer } from "../styles/styledContainer";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";


const CurrencyList = () => {

  const [currencies, setCurrencies] = useState({});
  const [fetching, setFetching] = useState(true);
  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchCurrencies = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/currencies" + filterUrl, authenticationConfig()).then(response => {
          setCurrencies(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchCurrencies();
  }, [filterData]);

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  if (fetching) {
    return <PageSpinner />;
  }

  return (
    <StyledContainer>
      <Helmet>
        <title>Currency - X-Changer.ml</title>
      </Helmet>
      <StyledCurrencyWrapper>
        <Title value="Currency" />
        {(currencies.length === 0) ?
          <Alert severity="warning">Currencies are empty</Alert> :
          <StyledTable>
            <StyledTableHead col="2">
              <StyledColHead>
                Asset
              </StyledColHead>
              <StyledColHead>
                Rate
              </StyledColHead>
            </StyledTableHead>
            <StyledTableBody>
              {currencies.map((currency, key) => (
                <StyledRow
                  col="2"
                  key={key}
                >
                  <StyledCol data-title="Asset">
                    {currency.asset}
                  </StyledCol>
                  <StyledCol data-title="Rate">
                    {currency.course}
                  </StyledCol>
                </StyledRow>
              ))}
            </StyledTableBody>
          </StyledTable>}
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledCurrencyWrapper>
    </StyledContainer>
  );
};

export default CurrencyList;
