import React from "react";
import Alert from "@material-ui/lab/Alert";
import Spinner from "../spinner/Spinner";

import { StyledPaymentSystemList } from "./styledPaymentSystem";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";

const PaymentSystemList = ({ fetching, paymentSystems }) => {

  if (fetching) {
    return <Spinner/>;
  }

  return (
    <StyledPaymentSystemList>
      {(paymentSystems.length === 0) ?
        <Alert severity="warning">Payment systems are empty</Alert> :
        <StyledTable className="payment-system-table">
          <StyledTableHead col="3" className="payment-system-table__head">
            <StyledColHead />
            <StyledColHead>
              Name
            </StyledColHead>
            <StyledColHead>
              Code
            </StyledColHead>
          </StyledTableHead>
          <StyledTableBody>
            {paymentSystems.map((paymentSystem, key) => (
              <StyledRow col="3" key={key} className="payment-system-table__row">
                <StyledCol data-title="Иконка" className="payment-system-table__icon">
                  <span className={`exchange-icon-${paymentSystem.code}`} />
                </StyledCol>
                <StyledCol data-title="Name" className="payment-system-table__name">
                  {paymentSystem.name}
                </StyledCol>
                <StyledCol data-title="Code" className="payment-system-table__code">
                  {paymentSystem.code}
                </StyledCol>
              </StyledRow>
            ))}
          </StyledTableBody>
        </StyledTable>}
    </StyledPaymentSystemList>
  );
};

export default PaymentSystemList;
