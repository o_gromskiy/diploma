import styled from "styled-components";

export const StyledPaymentSystemWrapper = styled.div`
  padding: 25px 0;
`;

export const StyledPaymentSystemForm = styled.form`
  padding: 20px 15px;
  display: grid;
  grid-template-columns: 256px 100px max-content;
  grid-gap: 15px;
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  @media screen and (max-width: 768px) {
    grid-template-columns: 100%;
  }
`;

export const StyledPaymentSystemList= styled.div`
  padding: 25px 0;
  .payment-system-table {
    &__head, &__row {
      grid-template-columns: 25px 300px max-content;
      grid-gap: 30px;
    }

    @media screen and (max-width: 992px) {
      &__icon {
        grid-area: tagIcon;
      }
      &__name {
        grid-area: name;
      }
      &__code {
        grid-area: tagCode;
      }
      &__row {
        padding: 15px;
        grid-gap: 15px;
        grid-template-columns: repeat(2, 1fr);
        grid-template-rows: repeat(2, 1fr);
        grid-template-areas: 'name name'
                             'tagIcon tagCode';
      }
    }
  }
`;