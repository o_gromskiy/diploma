import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import PaymentSystemList from "./PaymentSystemList";
import PaymentSystemForm from "./PaymentSystemForm";
import Title from "../title/Title";

import { StyledContainer } from "../styles/styledContainer";
import { StyledPaymentSystemWrapper } from "./styledPaymentSystem";
import { useHistory } from "react-router-dom";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import MaterialPagination from "../styles/materialPagination";
import { Helmet } from "react-helmet-async";
import Notification from "../notification/notification";

const PaymentSystemContainer = () => {

  const [visible, setVisible] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
    message: ""
  });

  const [paymentSystems, setPaymentSystems] = useState({});
  const [fetching, setFetching] = useState(true);

  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchPaymentSystem = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/currency_payment_systems" + filterUrl, authenticationConfig()).then(response => {
          setPaymentSystems(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchPaymentSystem();
  }, [filterData]);

  const handleChangeAlert = (message, type) => {
    setVisible({ ...visible, open: true, type: type, message: message });
  };

  const handleCloseAlert = () => {
    setVisible({ ...visible, open: false });
  };

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  return (
    <StyledContainer>
      <Notification
        type={visible.type}
        visible={visible}
        close={handleCloseAlert}
        message={visible.message}
      />
      <Helmet>
        <title>Payment systems - X-Changer.ml</title>
      </Helmet>
      <StyledPaymentSystemWrapper>
        <Title value="Payment systems" />
        <PaymentSystemForm
          paymentSystems={paymentSystems}
          setPaymentSystems={setPaymentSystems}
          handleChangeAlert={handleChangeAlert}
        />
        <PaymentSystemList
          fetching={fetching}
          paymentSystems={paymentSystems}
        />
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledPaymentSystemWrapper>
    </StyledContainer>
  );
};

export default PaymentSystemContainer;
