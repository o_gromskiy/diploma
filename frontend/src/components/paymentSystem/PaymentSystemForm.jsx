import React from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { CustomTextfield } from "../textfield/CustomTextfield";

import { StyledPaymentSystemForm } from "./styledPaymentSystem";
import MyButton from "../styles/materialButtonStyle";

const PaymentSystemForm = ({ paymentSystems, setPaymentSystems }) => {

  const handleSubmit = (e) => {
    e.preventDefault();

    let target = e.target;

    let data = {
      name: target.name.value,
      code: target.code.value
    };

    axios.post(`/api/currency_payment_systems`, data, authenticationConfig()).then(response => {
      if (response.status === 201) {
        target.reset();
        paymentSystems = [response.data, ...paymentSystems];
        setPaymentSystems(paymentSystems);
      }
    }).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  return (
    <StyledPaymentSystemForm onSubmit={handleSubmit}>
      <CustomTextfield
        label="Name"
        id="name"
        name="name"
        type="text"
      />
      <CustomTextfield
        label="Code"
        id="code"
        name="code"
        type="text"
      />
      <div className="payment-system__form-action">
        <MyButton color="success" aria-haspopup="true" type="submit">
          Create
        </MyButton>
      </div>
    </StyledPaymentSystemForm>
  );
};

export default PaymentSystemForm;
