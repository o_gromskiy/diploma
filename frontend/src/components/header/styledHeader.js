import styled from 'styled-components'

export const StyledHeaderWrapper = styled.header`
  padding: 10px 0;
  background-color: #fff;
  box-shadow: 0 1px 3px rgba(0,0,0,.45);
  position: relative;
`;

export const StyledHeaderContent = styled.div`
  display: grid;
  grid-template-columns: 45px 1fr;
  grid-gap: 15px;
  align-items: center;
  @media screen and (max-width: 768px) {
    grid-template-columns: 45px 30px;
    justify-content: space-between;
  }
`;

export const StyledHamburgerButton = styled.button`
  padding: 2px 3px;
  color: #2DB37F;
  font-size: 26px;
  border: none;
  background-color: transparent;
  transition: all .3s ease;
  display: none;
  justify-content: center;
  align-items: center;
  &:hover {
    color: #008D75;
    transform: scale(0.98);
  }
  @media screen and (max-width: 768px) {
    display: flex;
  }
`;