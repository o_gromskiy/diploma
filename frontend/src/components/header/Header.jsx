import React, { useState } from "react";
import Logo from "../logo/Logo";
import Navigation from "../navigation/Navigation";

import { StyledHamburgerButton, StyledHeaderContent, StyledHeaderWrapper } from "./styledHeader";
import { StyledContainer } from "../styles/styledContainer";

const Header = ({ authentication, setAuthentication }) => {

  const [hide, setHide] = useState(true);

  const toggleMenu = () => {
    setHide(!hide);
  };

  return (
    <StyledHeaderWrapper>
      <StyledContainer wrapper="content">
        <StyledHeaderContent>
          <Logo />
          <StyledHamburgerButton onClick={toggleMenu}>
            <span className="icon-bar" />
          </StyledHamburgerButton>
          <Navigation
            authentication={authentication}
            setAuthentication={setAuthentication}
            hide={hide}
            toggleMenu={toggleMenu}
          />
        </StyledHeaderContent>
      </StyledContainer>
    </StyledHeaderWrapper>
  );
};

export default Header;