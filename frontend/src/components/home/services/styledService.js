import styled from "styled-components";

export const StyledServicesWrapper = styled.div`
  padding: 50px 0 30px;
  @media screen and (max-width: 992px) {
    padding-bottom: 15px;
  }
`;

export const StyledServiceContent = styled.div`
  padding: 50px 0 0;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 30px;
  @media screen and (max-width: 992px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 15px;
  }
  @media screen and (max-width: 576px) {
    grid-template-columns: 100%;
  }
`;

export const StyledServiceItem = styled.div`
  padding: 25px 15px;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 100px 45px max-content;
  justify-content: center;
  grid-gap: 15px;
  background-color: rgba(45,179,127,0.1);
  box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0px rgb(0 0 0 / 14%), 0 1px 3px 0px rgb(0 0 0 / 12%);
  transition: all .3s ease;
  &:hover {
    transform: scale(1.04);
    .service-item__icon {
      transform: scale(1.05);
    }
  }
  @media screen and (max-width: 992px) {
    grid-template-rows: 100px repeat(2, max-content);
  }
  .service-item__icon {
    height: 100px;
    width: 100px;
    margin: 0 auto;
    transition: all .3s ease;
    img {
      width: 100%;
      height: 100%;
      object-fit: contain;
      object-position: center;
    }
  }
  .service-item__title {
    color: #2DB37F;
    font-size: 18px;
    font-weight: 700;
    text-align: center;
  }
  .service-item__text {
    line-height: 22px;
    text-align: center;
  }
`;