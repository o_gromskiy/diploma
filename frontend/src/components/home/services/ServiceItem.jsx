import React from "react";
import { StyledServiceItem } from "./styledService";

const ServiceItem = ({ title, text, img }) => {
  return (
    <StyledServiceItem>
      <div className="service-item__icon">
        <img
          src={img}
          alt=""
        />
      </div>
      <h4 className="service-item__title">
        {title}
      </h4>
      <p className="service-item__text">
        {text}
      </p>
    </StyledServiceItem>
  );
};

export default ServiceItem;