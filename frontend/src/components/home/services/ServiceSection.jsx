import React from "react";
import ServiceItem from "./ServiceItem";

import favorableRate from "../../../assets/images/favorable-rate.svg";
import manyDirections from "../../../assets/images/many-directions.svg";
import safety from "../../../assets/images/safety.svg";
import speed from "../../../assets/images/speed.svg";

import { StyledSectionTitle } from "../../../pages/home/styledHome";
import { StyledServiceContent, StyledServicesWrapper } from "./styledService";
import { StyledContainer } from "../../styles/styledContainer";

const ServiceSection = () => {
  return (
    <StyledServicesWrapper>
      <StyledContainer wrapper="content">
        <StyledSectionTitle>
          High level of service
        </StyledSectionTitle>
        <StyledServiceContent>
          <ServiceItem
            img={favorableRate}
            title="Favorable exchange rate"
            text="We find the most advantageous rate of world cryptocurrency exchanges for you"
          />
          <ServiceItem
            img={manyDirections}
            title="Many directions of exchange"
            text="In a few clicks"
          />
          <ServiceItem
            img={speed}
            title="Speed"
            text="Automatic exchange from a few seconds"
          />
          <ServiceItem
            img={safety}
            title="Safety"
            text="Multi-level protection of the transaction and your funds"
          />
        </StyledServiceContent>
      </StyledContainer>
    </StyledServicesWrapper>
  );
};

export default ServiceSection;
