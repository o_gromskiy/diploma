import React from "react";
import AliceCarousel from "react-alice-carousel";
import ReviewItem from "./ReviewItem";
import "react-alice-carousel/lib/alice-carousel.css";

import userImage from "../../../assets/images/user.svg"

import { StyledReviewsWrapper } from "./styledReviews";
import { StyledContainer } from "../../styles/styledContainer";
import { StyledSectionTitle } from "../../../pages/home/styledHome";

const ReviewsSection = () => {
  const SampleNextArrow = () => {return <span className="icon-chevron-right" />;};
  const SamplePrevArrow = () => { return <span className="icon-chevron-left" />; };

  const items = [
    <ReviewItem
      name="Ivan"
      text="Convenient service for cashless exchange."
      photo={userImage} />,
    <ReviewItem
      name="Yura"
      text="Already more than 10 exchanges, no complaints."
      photo={userImage} />,
    <ReviewItem
      name="Valera"
      text="Prompt exchanger with responsive support, I liked everything. Favorable rates compared to other exchangers, so I recommend it."
      photo={userImage} />,
    <ReviewItem
      name="Pavel"
      text="I managed to make an exchange at the peak of the fall of the cue ball, they worked quickly. I will come back here again!"
      photo={userImage} />
  ];

  return (
    <StyledReviewsWrapper>
      <StyledContainer wrapper="content">
        <StyledSectionTitle>
          We are trusted
        </StyledSectionTitle>
        <AliceCarousel
          autoPlay
          infinite
          mouseTracking
          items={items}
          autoPlayInterval={5000}
          animationDuration={1000}
          renderNextButton={SampleNextArrow}
          renderPrevButton={SamplePrevArrow}
        />
      </StyledContainer>
    </StyledReviewsWrapper>
  );
};

export default ReviewsSection;