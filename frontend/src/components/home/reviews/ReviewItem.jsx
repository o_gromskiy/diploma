import React from "react";
import { StyledReviewItem } from "./styledReviews";

const ReviewItem = ({ name, text, photo }) => {
  return (
    <StyledReviewItem>
      <p className="review__message">
        “{text}”
      </p>
      <div className="review__author">
        {name}
      </div>
      <div className="review__photo">
        <img src={photo} alt="user-photo" />
      </div>
    </StyledReviewItem>
  );
};

export default ReviewItem;