import styled from "styled-components";

export const StyledRatesChartWrapper = styled.div`
  padding: 80px 0 0;
`;

export const StyledRatesChartActions = styled.div`
  padding-bottom: 15px;
  display: grid;
  grid-template-columns: repeat(3, max-content);
  align-items: end;
  .default-spinner {
    padding: 8px;
    opacity: 0.25;
  }
  button {
    margin: 8px;
  }
  @media screen and (max-width: 576px) {
    grid-template-columns: 100%;
  }
`;


export const StyledRatesChartGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  grid-gap: 30px;
  @media screen and (max-width: 992px) {
    grid-template-columns: 100%;
    grid-gap: 0;
  }
`;

export const StyledRatesChartMain = styled.div`
  display: grid;
  grid-template-columns: 100%;
`;

export const StyledChartItemWrapper = styled.div`
  margin-bottom: 30px;
  padding: 15px;
  background-color: #fff;
  box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0px rgb(0 0 0 / 14%), 0 1px 3px 0px rgb(0 0 0 / 12%);
  @media screen and (max-width: 992px) {
    margin-bottom: 15px;
  }
`;

export const StyledChartItemTitle = styled.h4`
  margin-bottom: 15px;
  color: ${({color}) => color};
  font-size: 18px;
  text-align: center;
`;

