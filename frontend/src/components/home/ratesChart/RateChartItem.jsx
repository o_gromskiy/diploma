import React, { useEffect, useState } from "react";
import axios from "axios";
import { TimestampToDateTime } from "../../../utils/timestampToDate";
import Forbidden from "../../exceptions/forbidden/Forbidden";
import Spinner from "../../spinner/Spinner";
import AlertMessage from "../../alert/Alert";
import { Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";

import { StyledChartItemTitle, StyledChartItemWrapper } from "./styledRatesChart";

const RateChartItem = ({ url, name, fetching }) => {

  const [rates, setRates] = useState(null);

  const [range, setState] = useState({
    min: 0,
    max: 100,
    limit: 100
  });

  const prepareRatesForChart = (data) => {
    let fetchedData = [];

    let max = 0;
    let min = 1000000;
    let limit = 0;

    data.forEach((value, key) => {

      if (value.rate > max) {
        max = value.rate;
      }

      if (value.rate < min) {
        min = value.rate;
      }

      fetchedData.push({ name: TimestampToDateTime(value.time), USD: value.rate });

    });

    limit = (Number(max) - Number(min)) / 2;

    setState({
      min: Number(Number(min) - Number(limit)).toFixed(6),
      max: Number(Number(max) + Number(limit)).toFixed(6),
      limit: limit.toFixed(2)
    });

    return fetchedData;
  };

  const fetchRates = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get(url).then(response => {
          if (response.status === 200) {
            setRates(prepareRatesForChart(response.data));
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchRates();
  }, [url, fetching]);

  if (!rates) {
    return <Spinner
      display="block"
      size="35px"
    />;
  }

  if (!rates || rates.length === 0) {
    return <AlertMessage
      type="info"
      message="Rates data are empty"
    />;
  }

  return (
    <StyledChartItemWrapper>
      <StyledChartItemTitle color={name === "Forecast" ? "#27448d" : "#008D75"}>
        {name}
      </StyledChartItemTitle>
      <ResponsiveContainer width="100%" height="100%" maxHeight="300px">
        <AreaChart
          width={360}
          height={300}
          data={rates}
        >
          <Area
            type="monotone"
            dataKey="USD"
            stroke={name === "Forecast" ? "#27448d" : "#008D75"}
            fill={name === "Forecast" ? "#e3f1ff" : "#e1eee9"}
            />
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
          <XAxis dataKey="name" />
          <YAxis domain={[Number(range.min), Number(range.max)]} />
          <Tooltip />
        </AreaChart>
      </ResponsiveContainer>
    </StyledChartItemWrapper>
  );
};

export default RateChartItem;
