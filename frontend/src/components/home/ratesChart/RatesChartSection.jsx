import React, { useState } from "react";
import RateChartItem from "./RateChartItem";
import CryptocurrencySelect from "../../cryptocurrency/CryptocurrencySelect";
import Forbidden from "../../exceptions/forbidden/Forbidden";
import axios from "axios";

import {
  StyledRatesChartActions,
  StyledRatesChartGrid,
  StyledRatesChartMain,
  StyledRatesChartWrapper
} from "./styledRatesChart";
import { StyledContainer } from "../../styles/styledContainer";
import MyButton from "../../styles/materialButtonStyle";
import { StyledSectionTitle } from "../../../pages/home/styledHome";
import Spinner from "../../spinner/Spinner";

const RatesChartSection = () => {
  const [cryptocurrency, setCryptocurrency] = useState("BTC");
  const [fetching, setFetching] = useState(false);
  const [loading, setLoading] = useState(false);

  const createForecast = () => {
    let isMounted = true;

    if (isMounted) {
      setLoading(true);
      axios.post("/api/update-forecast-rates").then(response => {
          if (response.status === 200) {
            setFetching(!fetching);
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      }).finally(()=> setLoading(false));
    }

    return () => {
      isMounted = false;
    };
  };

  return (
    <StyledRatesChartWrapper>
      <StyledContainer wrapper="content">
        <StyledSectionTitle>
          Cryptocurrency analysis
        </StyledSectionTitle>
        <StyledRatesChartActions>
          <CryptocurrencySelect
            cryptocurrency={cryptocurrency}
            setCryptocurrency={setCryptocurrency}
          />
          <MyButton
            disabled={loading}
            color="success"
            onClick={createForecast}>
            Create forecast
          </MyButton>
          {loading && <Spinner />}
        </StyledRatesChartActions>
        <StyledRatesChartGrid>
          <RateChartItem
            url={"/api/huobi_rates?asset=" + cryptocurrency}
            name="Huobi"
            fetching={fetching}
          />
          <RateChartItem
            url={"/api/white_bit_rates?asset=" + cryptocurrency}
            name="WhiteBit"
            fetching={fetching}
          />
          <RateChartItem
            url={"/api/binance_rates?asset=" + cryptocurrency}
            name="Binance"
            fetching={fetching}
          />
        </StyledRatesChartGrid>
        <StyledRatesChartMain>
          <RateChartItem
            url={"/api/forecast_rates?asset=" + cryptocurrency}
            name="Forecast"
            fetching={fetching}
          />
        </StyledRatesChartMain>
      </StyledContainer>
    </StyledRatesChartWrapper>
  );

};

export default RatesChartSection;
