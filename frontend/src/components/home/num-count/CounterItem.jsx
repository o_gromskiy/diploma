import React, { useState } from "react";
import VisibilitySensor from "react-visibility-sensor";
import CountUp from "react-countup";

import { StyledCounterItem } from "./styledCounter";

const CounterItem = ({ top, bottom, img, endCount, countSuffix }) => {
  const [countUp, setCountUp] = useState(false);

  const visibilityChange = (isVisible) => {
    if (isVisible) {
      setCountUp(true);
    }
  };

  return (
    <StyledCounterItem style={{ backgroundImage: `url(${img})` }}>
      <div className="counter-item__top">
        {top}
      </div>
      <div className="counter-item__num">
        <VisibilitySensor
          onChange={visibilityChange}
          offset={{ top: 10 }}
        >
          <CountUp
            start={0}
            end={countUp ? endCount : 0}
            duration={5}
            suffix={countSuffix}
          />
        </VisibilitySensor>
      </div>
      <div className="counter-item__bottom">
        {bottom}
      </div>
    </StyledCounterItem>
  );
};

export default CounterItem;