import React from "react";
import SolutionItem from "./SolutionItem";

import { StyledSolutionContent, StyledSolutionWrapper } from "./styledSolution";
import { StyledSectionTitle } from "../../../pages/home/styledHome";
import { StyledContainer } from "../../styles/styledContainer";

const SolutionSection = () => {
  return (
    <StyledSolutionWrapper>
      <StyledContainer wrapper="content">
        <StyledSectionTitle>
          Ideal solution for:
        </StyledSectionTitle>
        <StyledSolutionContent>
          <SolutionItem text="Beginners and one-time deals" />
          <SolutionItem text="Constant / periodic exchange of those who have income in the crypt - freelancers, miners, individual entrepreneurs" />
          <SolutionItem text="Profitable financial transactions for traders, investors and those who earn in crypto" />
          <SolutionItem text="For long-term savings, buy crypto as part of a multi-currency basket" />
          <SolutionItem text="For economical purchases abroad, in online stores, payment for hosting and other services" />
          <SolutionItem text="For profitable cross-border transfers for any amount" />
        </StyledSolutionContent>
        <StyledSectionTitle>
          Anyone who keeps up with the times and wants to try crypto as a trend and innovation
        </StyledSectionTitle>
      </StyledContainer>
    </StyledSolutionWrapper>
  );
};

export default SolutionSection;