import React from "react";
import { StyledSolutionItem } from "./styledSolution";

const SolutionItem = ({ text }) => {
  return (
    <StyledSolutionItem>
      {text}
    </StyledSolutionItem>
  );
};

export default SolutionItem;