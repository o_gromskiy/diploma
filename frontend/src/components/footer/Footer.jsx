import React from "react";
import { StyledFooterWrapper } from "./styledFooter";

const Footer = () => {
  const currentYear = (new Date().getFullYear());

  return (
    <StyledFooterWrapper>
      <p>Oleksandr Hromskyi,</p>
      <p>{currentYear} р.</p>
    </StyledFooterWrapper>
  );
};

export default Footer;