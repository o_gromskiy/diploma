import styled from "styled-components";

export const StyledFooterWrapper = styled.footer`
  padding: 50px 0;
  text-align: center;
  background-color: #fff;
  box-shadow: 0 0 3px rgba(0,0,0,.45);
  p:not(:last-child) {
    padding-bottom: 10px;
  }
`;