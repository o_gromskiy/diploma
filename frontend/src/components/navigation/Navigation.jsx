import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import AdminNavList from "./AdminNavList";

import { StyledExit, StyledNavigation, StyledNavItem, StyledNavLink } from "./styledNavigation";

const Navigation = ({ authentication, setAuthentication, hide, toggleMenu }) => {

  const history = useHistory();

  const logout = () => {
    Cookies.remove("token");
    setAuthentication(false);
    history.push("/");
  };

  return (
    <StyledNavigation hide={hide}>
      <ul className="menu-list">
        <StyledNavItem>
          <StyledNavLink as={NavLink} exact={true} to="/" activeClassName="current-link" onClick={toggleMenu}>
            Exchange
          </StyledNavLink>
        </StyledNavItem>
        <StyledNavItem>
          <StyledNavLink as={NavLink} exact={true} to="/rates" activeClassName="current-link" onClick={toggleMenu}>
            Rates
          </StyledNavLink>
        </StyledNavItem>
        {authentication !== true ?
          <>
            <StyledNavItem>
              <StyledNavLink as={NavLink} to="/login" activeClassName="current-link" onClick={toggleMenu}>
                Login
              </StyledNavLink>
            </StyledNavItem>
            <StyledNavItem>
              <StyledNavLink as={NavLink} to="/registration" activeClassName="current-link" onClick={toggleMenu}>
                Registration
              </StyledNavLink>
            </StyledNavItem>
          </> :
          <>
            <AdminNavList toggleMenu={toggleMenu}/>
            <StyledNavItem>
              <StyledExit onClick={logout} type="button" title="Logout">
                <span className="icon icon-door-open" />
                <span className="text">Logout</span>
              </StyledExit>
            </StyledNavItem>
          </>}
      </ul>
    </StyledNavigation>
  );
};

export default Navigation;