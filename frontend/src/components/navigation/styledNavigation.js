import styled from "styled-components";

export const StyledNavigation = styled.nav`
  .current-link {
    color: #008D75;
    text-decoration: underline;
  }
  .menu-list {
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
  @media screen and (max-width: 768px) {
    ${({hide}) => hide && 'height: 0'};
    ${({hide}) => hide && 'display: none'};
    ${({hide}) => !hide && 'animation: loadContent .15s ease'};
    width: 100%;
    position: absolute;
    top: 65px;
    left: 0;
    background-color: #fff;
    box-shadow: 0 1px 3px rgba(0,0,0,.45);
    z-index: 100;
    .menu-list {
      flex-direction: column;
      li {
        margin: 0;
        padding: 10px 0;
        &:before {
          display: none;
        }
      }
    }
      @keyframes loadContent {
          0% {
              opacity: 0;
              transform: translateY(-25px);
          }
          100% {
              opacity: 1;
              transform: translateY(0px);
          }
      }
  }
`;

export const StyledNavItem = styled.li`
  position: relative;
  margin-right: 20px;
  &:before {
    content: '|';
    color: #2DB37F;
    position: absolute;
    top: 0;
    right: -12px;
    opacity: 0.35;
  }
  &:last-child {
    margin-right: 0;
    &:before {
      display: none;
    }
  }
`;

export const StyledNavLink = styled.div`
  width: 100%;
  color: #2DB37F;
  font-size: 16px;
  font-weight: 700;
  transition: all .1s ease;
  cursor: pointer;
  &:hover {
    color: #008D75;
  }
`;

export const StyledExit = styled.button`
  padding: 2px 3px;
  color: #2DB37F;
  background-color: rgba(45, 179, 127, 0.15);
  border: 1px solid #008D75;
  border-radius: 3px;
  transition: all .3s ease;
  .text {
    display: none;
  }
  &:hover {
    color: #008D75;
    transform: scale(0.98);
  }
  @media screen and (max-width: 768px){
    padding: 0;
    font-size: 16px;
    font-weight: 700;
    background-color: transparent;
    border: none;
    .text {
      display: block;
    }
    .icon {
      display: none;
    }
  }
`;