import React from 'react';

import { StyledBreadcrumbItem, StyledBreadcrumbLink } from '../styles/styledBreadcrumb';

const BreadcrumbItem = ({as, to, title}) => {
  return (
    <StyledBreadcrumbItem>
      <StyledBreadcrumbLink as={as} to={to}>
        {title}
      </StyledBreadcrumbLink>
    </StyledBreadcrumbItem>
  )
}

export default BreadcrumbItem