import React, { useEffect, useState } from "react";
import axios from "axios";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import Spinner from "../spinner/Spinner";
import removeDuplicatesFromArrayOfObjects from "../../utils/removeDuplicatesFromArrayOfObjects";
import isAuthenticated from "../../utils/isAuthenticated";
import authenticationConfig from "../../utils/authenticationConfig";
import MyButton from "../styles/materialButtonStyle";
import { CustomTextfield } from "../textfield/CustomTextfield";

import {
  StyledCalculatorAction,
  StyledCalculatorPairsWrapper,
  StyledCalculatorSwap,
  StyledCalculatorWrapper,
  StyledPairPart,
  StyledPairPartList,
  StyledPairPartTitle,
  StyledPairPartWrapper
} from "./styledCalculator";

const CalculatorContainer = () => {

  const [pairs, setPairs] = useState({});
  const [fetching, setFetching] = useState(true);

  const currency = "currency";
  const cryptocurrency = "cryptocurrency";

  const [type, setType] = useState(currency);

  const [openRequisites, setOpenRequisites] = useState(false);

  const [requisites, setRequisites] = useState({});

  const [currentPair, setCurrentPair] = useState();

  const [inCount, setInCount] = useState(1);
  const [outCount, setOutCount] = useState(1);

  const fetchPairs = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get("/api/exchange_pairs?active=true").then(response => {
          let data = response.data;
          setPairs(data);

          data.forEach(item => {
            if (item.payment.exchangeObject.type === type) {
              setCurrentPair(item);
              setFetching(false);
            }
          });
        }
      ).catch(error => {
        console.log(error);
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    let url = new URL(process.env.REACT_APP_MERCURE_URL);
    url.searchParams.append("topic", `create:pair`);

    const eventSource = new EventSource(url);

    eventSource.onmessage = (event) => {
      fetchPairs();
    };

    fetchPairs();

    return () => {
      eventSource.close();
    };
  }, []);

  useEffect(() => {
    if (currentPair) {
      setInCount(1);
      setOutCount(calculateCourse(currentPair));
    }
  }, [currentPair]);

  const handleOpenRequisites = () => {
    if (!isAuthenticated()) {
      window.location.href = "/login";
      return;
    }
    setOpenRequisites(true);
  };

  const handleCreateRequisition = () => {
    let data = {
      exchangePair: currentPair.id,
      payment: inCount,
      payout: outCount,
      requisites: requisites
    };

    axios.post("/api/requisitions", data, authenticationConfig()).then(response => {
      if (response.status === 201) {
        window.location.href = "/panel/requisition/details/" + response.data.id;
      }
    }).catch(error => {
//TODO message
    });
  };

  const handleChangeInCount = (event) => {
    setInCount(event.target.value);
    setOutCount(event.target.value * (calculateCourse(currentPair)));
  };

  const handleChangeOutCount = (event) => {
    setOutCount(event.target.value);
    setInCount(event.target.value * (calculateCourse(currentPair)));
  };

  const handleChangeLeft = (id, labels) => {
    let item = null;

    if (!id || !labels) {
      return;
    }

    setOpenRequisites(false);

    for (let value of pairs) {

      if (value.payment.exchangeObject.type === type && value.payment.id == id) {
        item = value;
      }
    }
    setCurrentPair(item);
  };

  const handleChangeRight = (id, labels) => {
    let item = null;

    if (!id || !labels) {
      return;
    }

    setOpenRequisites(false);

    for (let value of pairs) {

      if (value.payment.exchangeObject.type === type && value.payout.id == id) {
        item = value;
      }
    }

    setCurrentPair(item);
  };

  const handleChangeType = () => {
    setType(type === currency ? cryptocurrency : currency);
    setOpenRequisites(false);

    pairs.forEach(item => {
      if (item.payment.exchangeObject.type === (type === currency ? cryptocurrency : currency)) {
        setInCount(1);
        setCurrentPair(item);
        setOutCount(calculateCourse(item));
      }
    });

  };

  const renderCalculator = (pairs, type) => {
    let leftPart = [], rightPart = [];

    pairs.forEach(item => {
      if (item.payment.exchangeObject.type === type) {
        leftPart.push(item.payment);
        rightPart.push(item.payout);
      }
    });

    leftPart = removeDuplicatesFromArrayOfObjects(leftPart);
    rightPart = removeDuplicatesFromArrayOfObjects(rightPart);

    return (
      <StyledCalculatorPairsWrapper>
        <StyledPairPartWrapper>
          <StyledPairPartTitle>
            You give:
          </StyledPairPartTitle>
          <div>
            <StyledPairPartList onClick={(event) => handleChangeLeft(event.target.value, event.target.labels)}>
              {leftPart.map((value, key) => (
                <StyledPairPart
                  key={key}
                  htmlFor={value.id}
                  name="leftPartLabel"
                  className={value.id === currentPair.payment.id ? "exchange-pair_active" : ""}
                >
                  <div className={`exchange-icon-${value.paymentSystem.code}`} />
                  <div className="exchange-name">
                    {value.exchangeObject.asset} {type === currency && value.paymentSystem.name}
                  </div>
                  <input
                    id={value.id}
                    hidden
                    type="radio"
                    name="leftPart"
                    value={value.id}
                    defaultChecked={value.id === currentPair.payment.id}
                  />
                </StyledPairPart>
              ))}
            </StyledPairPartList>
            <CustomTextfield
              label="Amount to pay"
              id="inCount"
              type="text"
              value={inCount}
              onChange={handleChangeInCount}
              className="input-field"
            />
          </div>
        </StyledPairPartWrapper>

        <StyledCalculatorSwap>
          <label
            htmlFor="exchangeSwap"
            className="calculator-swap__btn"
          >
            <div className="icon-exchange" />
            <input
              hidden
              id="exchangeSwap"
              type="checkbox"
              onClick={handleChangeType}
            />
          </label>
        </StyledCalculatorSwap>

        <StyledPairPartWrapper>
          <StyledPairPartTitle>
            You get:
          </StyledPairPartTitle>
          <div>
            <StyledPairPartList onClick={(event) => handleChangeRight(event.target.value, event.target.labels)}>
              {rightPart.map((value, key) => (
                <StyledPairPart
                  key={key}
                  htmlFor={value.id}
                  name="rightPartLabel"
                  className={value.id === currentPair.payout.id ? "exchange-pair_active" : ""}
                >
                  <div className={`exchange-icon-${value.paymentSystem.code}`} />
                  <div className="exchange-name">
                    {value.exchangeObject.asset} {type === cryptocurrency && value.paymentSystem.name}
                  </div>
                  <input
                    id={value.id}
                    hidden
                    type="radio"
                    name="rightPart"
                    value={value.id}
                    defaultChecked={value.id === currentPair.payout.id}
                  />
                </StyledPairPart>
              ))}
            </StyledPairPartList>
            <CustomTextfield
              label="Amount to receive"
              id="outCount"
              type="text"
              value={outCount}
              onChange={handleChangeOutCount}
              className="input-field"
            />
          </div>
        </StyledPairPartWrapper>
      </StyledCalculatorPairsWrapper>
    );
  };

  const calculateCourse = (pair) => {
    let course = pair.payment.exchangeObject.course * pair.payout.exchangeObject.course;

    if (pair.payment.exchangeObject.type === currency) {
      course = 1 / course;
    }

    return course;
  };

  if (fetching) {
    return <Spinner />;
  }

  return (
    <StyledCalculatorWrapper>
      {renderCalculator(pairs, type)}
      <StyledCalculatorAction>
        {!openRequisites ?
          <div className="calculator-action__align-center">
            <MyButton
              color="success"
              onClick={handleOpenRequisites}
            >
              Exchange
            </MyButton>
          </div> :
          <div className="calculator-action__input-field">
            {type === cryptocurrency ?
              <CustomTextfield
                label="Card number"
                id="cardNumber"
                type="text"
                onChange={event => {
                  setRequisites({ ...requisites, cardNumber: event.target.value });
                }}
                className="input-field"
              /> :
              <CustomTextfield
                label="Wallet"
                id="wallet"
                type="text"
                onChange={event => {
                  setRequisites({ ...requisites, wallet: event.target.value });
                }}
                className="input-field"
              />
            }
            <div className="calculator-action__align-center">
              <MyButton
                color="success"
                onClick={handleCreateRequisition}
              >Confirm</MyButton>
            </div>
          </div>
        }
      </StyledCalculatorAction>
    </StyledCalculatorWrapper>
  );
};

export default CalculatorContainer;
