import styled from "styled-components";

export const StyledCalculatorWrapper = styled.div`
  padding: 15px;
  background-color: #fff;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  .input-field {
    width: 100%;
  }
`;

export const StyledCalculatorPairsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 100px 1fr;
  grid-gap: 15px;
  @media screen and (max-width: 768px) {
    grid-template-columns: 100%;
    grid-template-rows: max-content 50px max-content;
  }
`;

export const StyledCalculatorSwap= styled.div`
  padding: 40px 0 70px;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow: hidden;
  .calculator-swap__btn {
    width: 50px;
    height: 50px;
    color: #008D75;
    font-size: 28px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    transition: all .3s ease;
    cursor: pointer;
    &:hover {
      color: #2DB37F;
      transform: rotate(180deg);
    }
  }
  @media screen and (max-width: 768px) {
    padding: 0;
    border-radius: 3px;
    background-color: rgba(45,179,127,0.1);
    .calculator-swap__btn {
      width: 100%;
    }
  }
`;

export const StyledCalculatorAction = styled.div`
  padding-top: 25px;
  .input-field {
    margin-bottom: 25px;
  }
  .calculator-action__align-center {
    text-align: center;
  }
`;

export const StyledPairPartWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  & > div {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
  .input-field {
    margin-top: 25px;
    label {
      color: #008D75;
      font-weight: 700;
    }
  }
`;

export const StyledPairPartTitle = styled.h4`
  padding-bottom: 15px;
  color: #008D75;
  font-size: 16px;
`;

export const StyledPairPartList = styled.div`
  .exchange-pair_active {
    background-color: rgba(45,179,127,0.1);
    border-bottom: 1px solid #2DB37F;
  }
`;

export const StyledPairPart = styled.label`
  padding: 16px;
  display: grid;
  grid-template-columns: 25px 1fr;
  grid-gap: 15px;
  align-items: center;
  background-color: #fff;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  cursor: pointer;
  transition: all .3s ease;
  .exchange-name {
    font-weight: 700;
    text-transform: uppercase;
  }
  &:hover {
    transform: translateY(-5px);
    border-bottom: 1px solid #2DB37F;
    box-shadow: 0 1px 5px 0 rgba(0, 141, 117, 0.25);
  }
`;