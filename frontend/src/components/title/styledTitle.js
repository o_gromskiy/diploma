import styled from "styled-components";

export const StyledTitle = styled.div`
  padding: 15px 0 30px;
  h1 {
    color: #2DB37F;
    font-size: 24px;
  }
`;