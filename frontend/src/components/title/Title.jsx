import React from "react";

import { StyledTitle } from "./styledTitle";

const Title = ({ value }) => {
  return (
    <StyledTitle>
      <h1>
        {value}
      </h1>
    </StyledTitle>
  );
};

export default Title;