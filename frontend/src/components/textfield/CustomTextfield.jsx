import React from "react";

import { TextField } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

export const CustomTextfield = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#008D75"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#008D75"
    }
  }
})(TextField);