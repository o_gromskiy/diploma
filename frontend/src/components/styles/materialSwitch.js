import React from 'react';
import Switch from '@material-ui/core/Switch';

import { withStyles } from '@material-ui/styles';

const MaterialSwitch = withStyles({
  switchBase: {
    color: "#909090",
    '&$checked': {
      color: '#29c59c',
    },
    '&$checked + $track': {
      backgroundColor: '#29c59c',
    },
  },
  checked: {},
  track: {
    backgroundColor: '#909090',
  },
})(Switch);

export default MaterialSwitch;