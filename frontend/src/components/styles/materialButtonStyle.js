import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles({
  root: {
    background: ({color}) =>
      color === 'success'
        ? 'linear-gradient(45deg, #277661 30%, #29DEAE 90%)' :
      color === 'danger'
        ? 'linear-gradient(45deg, #FF0000 30%, #FB4B6B 90%)' :
      color === 'warning'
        ? 'linear-gradient(45deg, #FF7A00 30%, #FFBB38 90%)'
        : 'linear-gradient(45deg, #3f51b5 30%, #62a0fe 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: ({color}) =>
      color === 'success'
        ? '0 3px 5px 2px rgba(39, 118, 97, .3)' :
      color === 'danger'
        ? '0 3px 5px 2px rgba(230, 71, 106, .3)' :
      color === 'warning'
        ? '0 3px 5px 2px rgba(255, 122, 0, .3)'
        : '0 3px 5px 2px rgba(63, 81, 181, .3)',
    color: 'white',
    height: 44,
    padding: '0 15px',
    fontSize: '14px'
  },
});

function MyButton(props) {
  const {color, ...other} = props;
  const classes = useStyles(props);
  return <Button className={classes.root} {...other} />;
}

MyButton.propTypes = {
  color: PropTypes.oneOf(['primary', 'danger', 'warning', 'success']).isRequired,
};

export default MyButton;