import styled from 'styled-components';

export const StyledTable = styled.div`
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
`;

export const StyledTableHead = styled.div`
  background-color: #fff;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(${({col}) => col}, 1fr);
  @media screen and (max-width: 992px) {
    display: none;
  }
`;

export const StyledColHead = styled.div`
  padding: 16px;
  font-weight: 700;
`;

export const StyledTableBody = styled.div`
`;

export const StyledRow = styled.div`
  background-color: #fff;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: repeat(${({col}) => col}, 1fr);
  @media screen and (max-width: 992px) {
    padding: 15px;
    grid-template-rows: repeat(${({col}) => col}, minmax(50px, auto));
    grid-template-columns: 1fr;
  }
`;

export const StyledCol = styled.div`
  padding: 16px;
  display: ${({ inline }) => inline ? "inline-flex" : "grid"};
  align-items: center;
  position: relative;
  &:before {
    content: attr(data-title)':';
    font-size: 11px;
    letter-spacing: 0.4px;
    opacity: .5;
    position: absolute;
    top: 0;
    left: 0;
    display: none;
    @media screen and (max-width: 992px) {
      display: block;
    }
  }
  @media screen and (max-width: 992px) {
      margin-bottom: 5px;
      padding: 16px 0 0;
      align-items: start;
    }
`;