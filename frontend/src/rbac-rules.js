import {
  clients,
  cryptocurrency,
  currency,
  exchangePairs,
  navbar,
  paymentSystem,
  rates,
  requisition
} from "./rbac-consts";

const rules = {
  client: {
    static: [
      requisition.READ,
      requisition.DETAILS,
      requisition.PAID,
      requisition.REJECT,
      rates.READ
    ]
  },
  admin: {
    static: [
      navbar.ADMIN_PANEL,
      requisition.READ,
      requisition.DETAILS,
      requisition.REJECT,
      requisition.CLIENTS,
      currency.READ,
      cryptocurrency.READ,
      paymentSystem.READ,
      paymentSystem.WRITE,
      clients.READ,
      exchangePairs.READ,
      exchangePairs.WRITE,
      rates.READ
    ]
  }
};

export default rules;
