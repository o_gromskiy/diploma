const requisition = {
  READ: "requisitions:list",
  DETAILS: "requisitions:details",
  PAID: "requisitions:paid",
  REJECT: "requisitions:reject",
  CLIENTS: "requisitions:clients"
};

const currency = {
  READ: "currency:list"
};

const rates = {
  READ: "rates:list"
};

const cryptocurrency = {
  READ: "cryptocurrency:list"
};

const paymentSystem = {
  READ: "payment:system:list",
  WRITE: "payment:system:write"
};

const exchangePairs = {
  READ: "exchange:pairs:list",
  WRITE: "exchange:pairs:write"
};

const clients = {
  READ: "clients:list"
};

const navbar = {
  ADMIN_PANEL: "navbar:admin-panel"
};

export {
  requisition,
  currency,
  cryptocurrency,
  paymentSystem,
  clients,
  exchangePairs,
  navbar,
  rates
};
