<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      itemOperations={
 *          "get"
 *     },
 *     collectionOperations={
 *         "get"
 *     }
 * )
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"cryptocurrency"="Cryptocurrency", "currency"="Currency"})
 */
abstract class ExchangeObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get", "requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item"})
     */
    protected ?int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get", "requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item"})
     */
    protected string $asset;

    /**
     * @ORM\Column(type="float")
     * @Groups({"get", "requisition:collection", "requisition:item", "exchange:pair:collection"})
     */
    protected float $course;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get", "requisition:collection", "requisition:item"})
     */
    protected string $name;

    /**
     * @ORM\OneToMany(targetEntity=ExchangePart::class, mappedBy="exchangeObject")
     */
    private Collection $exchangeParts;

    /**
     * @Groups({"get", "requisition:collection", "requisition:item", "exchange:pair:collection"})
     */
    private $type;

    /**
     * ExchangeObject constructor.
     */
    public function __construct()
    {
        $this->exchangeParts = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getCourse(): float
    {
        return $this->course;
    }

    /**
     * @param float $course
     * @return ExchangeObject
     */
    public function setCourse(float $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return string
     */
    public function getAsset(): string
    {
        return $this->asset;
    }

    /**
     * @param string $asset
     * @return ExchangeObject
     */
    public function setAsset(string $asset): self
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection|ExchangePart[]
     */
    public function getExchangeParts(): Collection
    {
        return $this->exchangeParts;
    }

    /**
     * @param ExchangePart $exchangePart
     * @return $this
     */
    public function addExchangePart(ExchangePart $exchangePart): self
    {
        if (!$this->exchangeParts->contains($exchangePart)) {
            $this->exchangeParts[] = $exchangePart;
            $exchangePart->setExchangeObject($this);
        }

        return $this;
    }

    /**
     * @param ExchangePart $exchangePart
     * @return $this
     */
    public function removeExchangePart(ExchangePart $exchangePart): self
    {
        if ($this->exchangeParts->removeElement($exchangePart)) {
            // set the owning side to null (unless already changed)
            if ($exchangePart->getExchangeObject() === $this) {
                $exchangePart->setExchangeObject(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}
