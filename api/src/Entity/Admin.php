<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 */
class Admin extends User
{
    /**
     * @return mixed
     */
    public function getRoles()
    {
        $this->roles[] = self::ADMIN;

        return array_unique($this->roles);
    }
}
