<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ExchangePairRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ExchangePair\ExchangePairWriteActionController;
use App\Controller\ExchangePair\ExchangePairUpdateActionController;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @ApiResource(
 *     attributes={"order"={"id": "DESC"}},
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"exchange:pair:item"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "put"={
 *              "normalization_context"={"groups"={"exchange:pair:item"}},
 *              "denormalization_context"={"groups"={"exchange:pair:update"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "controller"=ExchangePairUpdateActionController::class
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"exchange:pair:collection"}},
 *          },
 *          "get-public"={
 *              "method" = "GET",
 *              "normalization_context"={"groups"={"exchange:pair:collection"}},
 *              "formats"= {"jsonld"},
 *              "path" = "/public/exchange_pairs",
 *          },
 *         "post"={
 *               "normalization_context"={"groups"={"exchange:pair:item"}},
 *               "denormalization_context"={"groups"={"exchange:pair:write"}},
 *               "security"="is_granted('ROLE_ADMIN')",
 *               "controller"=ExchangePairWriteActionController::class
 *         },
 *     }
 * )
 * @ORM\Entity(repositoryClass=ExchangePairRepository::class)
 * @ApiFilter(BooleanFilter::class, properties={"active"})
 */
class ExchangePair
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"exchange:pair:collection", "exchange:pair:item"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=ExchangePart::class, inversedBy="exchangePairs")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item", "exchange:pair:write"})
     */
    private ?ExchangePart $payment;

    /**
     * @ORM\ManyToOne(targetEntity=ExchangePart::class, inversedBy="exchangePairs")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item", "exchange:pair:write"})
     */
    private ?ExchangePart $payout;

    /**
     * @ORM\Column(type="float")
     * @Groups({"requisition:collection", "exchange:pair:collection", "exchange:pair:item", "exchange:pair:update", "exchange:pair:write"})
     */
    private ?float $percent;

    /**
     * @var bool $deactivated
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"exchange:pair:collection", "exchange:pair:item", "exchange:pair:update"})
     */
    private bool $active;

    /**
     * @ORM\OneToMany(targetEntity=Requisition::class, mappedBy="exchnagePair")
     */
    private Collection $requisitions;

    /**
     * ExchangePair constructor.
     */
    public function __construct()
    {
        $this->active = true;
        $this->requisitions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ExchangePart|null
     */
    public function getPayment(): ?ExchangePart
    {
        return $this->payment;
    }

    /**
     * @param ExchangePart|null $payment
     * @return $this
     */
    public function setPayment(?ExchangePart $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return ExchangePart|null
     */
    public function getPayout(): ?ExchangePart
    {
        return $this->payout;
    }

    /**
     * @param ExchangePart|null $payout
     * @return $this
     */
    public function setPayout(?ExchangePart $payout): self
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPercent(): ?float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     * @return $this
     */
    public function setPercent(float $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return Collection|Requisition[]
     */
    public function getRequisitions(): Collection
    {
        return $this->requisitions;
    }

    /**
     * @param Requisition $requisition
     * @return $this
     */
    public function addRequisition(Requisition $requisition): self
    {
        if (!$this->requisitions->contains($requisition)) {
            $this->requisitions[] = $requisition;
            $requisition->setExchangePair($this);
        }

        return $this;
    }

    /**
     * @param Requisition $requisition
     * @return $this
     */
    public function removeRequisition(Requisition $requisition): self
    {
        if ($this->requisitions->removeElement($requisition)) {
            // set the owning side to null (unless already changed)
            if ($requisition->getExchangePair() === $this) {
                $requisition->setExchangePair(null);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return ExchangePair
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
