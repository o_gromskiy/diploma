<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     itemOperations={
 *         "get"
 *     },
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"get"}},
 *             "formats"= {"jsonld"}
 *          }
 *     }
 * )
 */
class Cryptocurrency extends ExchangeObject
{
    const TYPE = "cryptocurrency";

    /**
     * @return mixed
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}
