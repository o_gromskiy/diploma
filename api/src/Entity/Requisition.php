<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Requisition\RequisitionCollectionAction;
use App\Controller\Requisition\RequisitionWriteAction;
use App\Entity\Traits\TimestampTrait;
use App\Repository\RequisitionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     attributes={"order"={"createdAt": "ASC"}},
 *     itemOperations={
 *          "get"={
 *                  "normalization_context"={"groups"={"requisition:item"}},
 *                  "security"="(is_granted('ROLE_ADMIN')) or (is_granted('ROLE_CLIENT') and object.getClient() === user)"
 *            },
 *          "put"={
 *              "normalization_context"={"groups"={"requisition:item"}},
 *              "denormalization_context"={"groups"={"requisition:update"}},
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_CLIENT')"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"requisition:collection"}},
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_CLIENT')",
 *              "controller"=RequisitionCollectionAction::class,
 *              "formats"= {"jsonld"}
 *          },
 *          "post"={
 *              "normalization_context"={"groups"={"requisition:collection"}},
 *              "denormalization_context"={"groups"={"requisition:update"}},
 *              "security"="is_granted('ROLE_CLIENT')",
 *              "controller"=RequisitionWriteAction::class
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=RequisitionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Requisition
{
    const NEW = 'NEW';
    const PENDING = 'PENDING';
    const PAID = 'PAID';
    const PAID_OUT = 'PAID_OUT';
    const REJECT = 'REJECT';
    const ERROR = 'ERROR';
    const PAYMENT_PENDING = 'PAYMENT_PENDING';
    const PAYOUT_PENDING = 'PAYOUT_PENDING';

    use TimestampTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=ExchangePair::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?ExchangePair $exchangePair;

    /**
     * @ORM\Column(type="float")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?float $payment;

    /**
     * @ORM\Column(type="float")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?float $payout;

    /**
     * @ORM\Column(type="float")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?float $percent;

    /**
     * @ORM\Column(type="float")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?float $course;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"requisition:collection", "requisition:item", "requisition:update"})
     */
    private string $status;

    /**
     * @ORM\Column(type="text")
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?string $requisites;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"requisition:item"})
     */
    private ?string $info = null;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="requisitions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"requisition:collection", "requisition:item"})
     */
    private ?Client $client;

    /**
     * @ORM\Column(type="float")
     */
    private ?float $tmpCount = 0;

    /**
     * Requisition constructor.
     */
    public function __construct()
    {
        $this->status = self::NEW;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ExchangePair|null
     */
    public function getExchangePair(): ?ExchangePair
    {
        return $this->exchangePair;
    }

    /**
     * @param ExchangePair|null $exchangePair
     * @return $this
     */
    public function setExchangePair(?ExchangePair $exchangePair): self
    {
        $this->exchangePair = $exchangePair;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPayment(): ?float
    {
        return $this->payment;
    }

    /**
     * @param float $payment
     * @return $this
     */
    public function setPayment(float $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPayout(): ?float
    {
        return $this->payout;
    }

    /**
     * @param float $payout
     * @return $this
     */
    public function setPayout(float $payout): self
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPercent(): ?float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     * @return $this
     */
    public function setPercent(float $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCourse(): ?float
    {
        return $this->course;
    }

    /**
     * @param float $course
     * @return $this
     */
    public function setCourse(float $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequisites(): ?string
    {
        return $this->requisites;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function addRequisites(array $data): self
    {
        $tmp = json_decode($this->getRequisites(), true) ?? [];
        $this->setRequisites(json_encode(array_merge($data, $tmp), true));

        return $this;
    }

    /**
     * @param string $requisites
     * @return $this
     */
    public function setRequisites(string $requisites): self
    {
        $this->requisites = $requisites;

        return $this;
    }

    /**
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * @param Client|null $client
     * @return $this
     */
    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Requisition
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTmpCount(): ?float
    {
        return $this->tmpCount;
    }

    /**
     * @param float|null $tmpCount
     * @return Requisition
     */
    public function setTmpCount(?float $tmpCount): self
    {
        $this->tmpCount = $tmpCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @param string|null $info
     * @return Requisition
     */
    public function setInfo(?string $info): self
    {
        $this->info = $info;
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function addInfo(array $data): self
    {
        $tmp = json_decode($this->getInfo(), true) ?? [];
        $this->setInfo(json_encode(array_merge($data, $tmp), true));

        return $this;
    }
}
