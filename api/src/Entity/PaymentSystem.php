<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PaymentSystemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PaymentSystemRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap({"cryptocurrency"="CryptocurrencyPaymentSystem", "currency"="CurrencyPaymentSystem"})
 */
abstract class PaymentSystem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get", "currency:payment:system:collection", "currency:payment:system:item", "exchange:pair:collection", "exchange:pair:item"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "currency:payment:system:collection", "currency:payment:system:item", "currency:payment:system:write", "exchange:pair:collection", "exchange:pair:item"})
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "currency:payment:system:collection", "currency:payment:system:item", "currency:payment:system:write", "exchange:pair:collection", "exchange:pair:item"})
     */
    private ?string $code;

    /**
     * @ORM\OneToMany(targetEntity=ExchangePart::class, mappedBy="paymentSystem")
     */
    private Collection $exchangeParts;

    /**
     * PaymentSystem constructor.
     */
    public function __construct()
    {
        $this->exchangeParts = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|ExchangePart[]
     */
    public function getExchangeParts(): Collection
    {
        return $this->exchangeParts;
    }

    /**
     * @param ExchangePart $exchangePart
     * @return $this
     */
    public function addExchangePart(ExchangePart $exchangePart): self
    {
        if (!$this->exchangeParts->contains($exchangePart)) {
            $this->exchangeParts[] = $exchangePart;
            $exchangePart->setPaymentSystem($this);
        }

        return $this;
    }

    /**
     * @param ExchangePart $exchangePart
     * @return $this
     */
    public function removeExchangePart(ExchangePart $exchangePart): self
    {
        if ($this->exchangeParts->removeElement($exchangePart)) {
            // set the owning side to null (unless already changed)
            if ($exchangePart->getPaymentSystem() === $this) {
                $exchangePart->setPaymentSystem(null);
            }
        }

        return $this;
    }
}
