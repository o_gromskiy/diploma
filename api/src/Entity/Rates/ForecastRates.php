<?php

namespace App\Entity\Rates;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     attributes={
 *          "order"={"id": "DESC"},
 *          "pagination_items_per_page"=20
 *     },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"rates:item"}}
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"rates:collection"}}
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\Rates\ForecastRatesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ForecastRates extends AbstractRates
{

}
