<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     attributes={"order"={"id": "DESC"}},
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"currency:payment:system:item"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"currency:payment:system:collection"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "formats"= {"jsonld"}
 *          },
 *         "post"={
 *               "normalization_context"={"groups"={"currency:payment:system:item"}},
 *               "denormalization_context"={"groups"={"currency:payment:system:write"}},
 *               "security"="is_granted('ROLE_ADMIN')"
 *         },
 *     }
 * )
 */
class CurrencyPaymentSystem extends PaymentSystem
{

}
