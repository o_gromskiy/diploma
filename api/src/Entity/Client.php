<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     attributes={"order"={"createdAt": "DESC"}},
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"client:item"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *          "put"={
 *              "normalization_context"={"groups"={"client:item"}},
 *              "denormalization_context"={"groups"={"client:update"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"client:collection"}},
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "formats"= {"jsonld"}
 *          },
 *     }
 * )
 */
class Client extends User
{
    /**
     * @ORM\OneToMany(targetEntity=Requisition::class, mappedBy="client")
     */
    private Collection $requisitions;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->requisitions = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $this->roles[] = self::CLIENT;

        return array_unique($this->roles);
    }

    /**
     * @return Collection|Requisition[]
     */
    public function getRequisitions(): Collection
    {
        return $this->requisitions;
    }

    /**
     * @param Requisition $requisition
     * @return $this
     */
    public function addRequisition(Requisition $requisition): self
    {
        if (!$this->requisitions->contains($requisition)) {
            $this->requisitions[] = $requisition;
            $requisition->setClient($this);
        }

        return $this;
    }

    /**
     * @param Requisition $requisition
     * @return $this
     */
    public function removeRequisition(Requisition $requisition): self
    {
        if ($this->requisitions->removeElement($requisition)) {
            // set the owning side to null (unless already changed)
            if ($requisition->getClient() === $this) {
                $requisition->setClient(null);
            }
        }

        return $this;
    }

}
