<?php

namespace App\Security;

use App\Entity\Client;
use App\Entity\User;
use App\Service\JWT;
use App\Services\Registration\Web\ClientRegistration;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Google_Client;
use JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Class TokenAuthenticator
 * @package App\Security
 */
class GoogleAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var JWT
     */
    protected JWT $JWT;

    /**
     * @var Google_Client
     */
    private Google_Client $googleClient;


    /**
     * GoogleAuthenticator constructor.
     * @param Google_Client $googleClient
     * @param EntityManagerInterface $entityManager
     * @param JWT $JWT
     */
    public function __construct(
        Google_Client $googleClient,
        EntityManagerInterface $entityManager,
        JWT $JWT
    ) {
        $this->googleClient = $googleClient;
        $this->entityManager = $entityManager;
        $this->JWT = $JWT;
    }

    /**
     * @param Request $request
     * @return bool
     * @throws JsonException
     */
    public function supports(Request $request): bool
    {
        $response = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        return isset($response["token"]);
    }

    /**
     * @param Request $request
     * @return array
     * @throws JsonException
     */
    public function getCredentials(Request $request): array
    {
        $response = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        return [
            'token' => $response["token"]
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface|null
     * @throws Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        if (null === $credentials) {
            return null;
        }

        $token = $credentials['token'];

        $payload = $this->googleClient->verifyIdToken($token);

        if (!$payload) {
            return null;
        }

        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['email' => $payload['email']]);

        if (!$user) {
            $user = $this->registerClient($payload);
        }

        return $user;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        if ($user->getIsBanned()) {
            throw new AccessDeniedHttpException(
                'Ваш аккаунт заблокирован администрацией сайта'
            );
        }

        return is_array($this->googleClient->verifyIdToken($credentials['token']));
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return JsonResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        $user = $token->getUser();

        return new JsonResponse(['token' => $this->JWT->createUserAuthToken($user)]);
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse|Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        return new JsonResponse(['message' => 'Authentication Required'], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * @param array $payload
     * @return Client
     */
    private function registerClient(array $payload): Client
    {
        $client = new Client();
        $client->setEmail($payload['email']);
        $client->setFirstname($payload['given_name']);
        $client->setLastname($payload['family_name']);

        $this->entityManager->persist($client);
        $this->entityManager->flush();

        return $client;
    }
}
