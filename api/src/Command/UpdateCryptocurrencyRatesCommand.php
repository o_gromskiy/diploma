<?php

namespace App\Command;

use App\Entity\Cryptocurrency;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\NativeHttpClient;

class UpdateCryptocurrencyRatesCommand extends Command
{
    protected static $defaultName = 'update:cryptocurrency:rates';
    protected static $defaultDescription = 'Add a short description for your command';
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * UpdateCryptocurrencyRatesCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = new NativeHttpClient();

        $response = $client->request('GET', 'https://api.binance.com/api/v3/ticker/price');

        $marketData = $response->toArray();

        $cryptocurrencies = $this->entityManager->getRepository(Cryptocurrency::class)->findAll();

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        foreach ($cryptocurrencies as $cryptocurrency) {
            foreach ($marketData as $data) {
                if ($cryptocurrency->getAsset() . 'USDT' === $data['symbol']) {
                    $cryptocurrency->setCourse($data['price']);
                }
            }
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
