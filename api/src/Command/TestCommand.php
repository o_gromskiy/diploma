<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestCommand
 * @package App\Command
 */
class TestCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'test';

    /**
     * @var string
     */
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        dd(WhiteBit::getBalance('BTC'));
//        dd(WhiteBit::getWithdrawInfo('USDT','45634543524235'));
//        dd(WhiteBit::transferBalance('USDT','20'));
//        dd(WhiteBit::createOrder('TRX','20','34534443535435347'));
//        dd(WhiteBit::createWithdraw('USDT',40,'0xb7352b65ed4bb6b51aa2fbb228829db57494b1dd','45634543524235'));

        return Command::SUCCESS;
    }

}
