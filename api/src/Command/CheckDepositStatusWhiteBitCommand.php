<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckDepositStatusWhiteBitCommand extends Command
{
    protected static $defaultName = 'check:deposit:status:white:bit';

    protected static $defaultDescription = 'Add a short description for your command';

    /**
     * CheckDepositStatusWhiteBitCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
//        $requisitions = $this->entityManager->getRepository(Requisition::class)->findAll();
//        /**
//         * @var Requisition $requisition
//         */
//        foreach ($requisitions as $requisition) {
//            if ($requisition->getExchangePair()->getPayment()->getExchangeObject()->getType() == "cryptocurrency" && ($requisition->getStatus() == Requisition::NEW || $requisition->getStatus() == Requisition::PENDING)) {
//                $response = WhiteBit::getDepositInfo($requisition->getExchangePair()->getPayment()->getExchangeObject()->getAsset());
//
//                if (!$response) {
//                    continue;
//                }
//
//                if (empty($response['records'])) {
//                    continue;
//                }
//
//                if (time() - $response['records'][0]['createdAt'] > 300) {
//                    continue;
//                }
//
//                if ($response['records'][0]['status'] == 15) {
//                    $requisition->setStatus(Requisition::PENDING);
//                }
//
//                if ($response['records'][0]['status'] == 3 || $response['records'][0]['status'] == 7) {
//
//                    $requisition->setPayment($response['records'][0]['amount']);
//                    $requisition->setStatus(Requisition::PAID);
//
//                    $requisition->setCourse($requisition->getExchangePair()->getPayment()->getExchangeObject()->getCourse() / $requisition->getExchangePair()->getPayout()->getExchangeObject()->getCourse());
//
//                    $requisition->setPayout($requisition->getPayment() * $requisition->getCourse());
//
//                    //TODO Payout
//                }
//            }
//        }
//
//        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
