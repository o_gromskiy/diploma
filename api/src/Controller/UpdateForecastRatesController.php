<?php

namespace App\Controller;

use App\Service\Rates\BinanceRateService;
use App\Service\Rates\ForecastRateService;
use App\Service\Rates\HuobiRateService;
use App\Service\Rates\WhiteBitRateService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class UpdateForecastRatesController
 * @package App\Controller
 */
class UpdateForecastRatesController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * PaymentController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/update-forecast-rates", name="update_forecast_rates", methods={"POST"})
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function index(): JsonResponse
    {
        BinanceRateService::updateRate($this->entityManager);

        HuobiRateService::updateRate($this->entityManager);

        WhiteBitRateService::updateRate($this->entityManager);

        ForecastRateService::updateRate($this->entityManager);

        return new JsonResponse();
    }
}
