<?php

namespace App\Controller\ExchangePair;

use App\Entity\ExchangePair;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;

/**
 * Class ExchangePairUpdateActionController
 * @package App\Controller\ExchangePair
 */
class ExchangePairUpdateActionController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var PublisherInterface
     */
    private PublisherInterface $publisher;

    /**
     * ExchangePairWriteActionController constructor.
     * @param EntityManagerInterface $entityManager
     * @param PublisherInterface $publisher
     */
    public function __construct(EntityManagerInterface $entityManager, PublisherInterface $publisher)
    {
        $this->entityManager = $entityManager;
        $this->publisher = $publisher;
    }

    /**
     * @param ExchangePair $pair
     * @return ExchangePair
     */
    public function __invoke(ExchangePair $pair): ExchangePair
    {
        $this->entityManager->persist($pair);

        $this->entityManager->flush();

        try {
            $update = new Update(
                'create:pair'
            );

            $publisher = $this->publisher;

            $publisher($update);
        } catch (Exception $exception) {
            return $pair;
        }

        return $pair;
    }
}
