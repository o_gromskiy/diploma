<?php


namespace App\Controller;

use App\Entity\Cryptocurrency;
use App\Entity\Currency;
use App\Entity\Requisition;
use App\Service\Signature;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PaymentCallbackController
 * @package App\Controller
 */
class PaymentCallbackController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * PaymentCallbackController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/payment-status-check/{id}", name="payment_status_check")
     * @param Request $request
     * @param Requisition $requisition
     * @return Response
     * @throws Exception
     */
    public function paymentStatusCheck(Request $request, Requisition $requisition)
    {
        $content = json_decode($request->getContent(), true);

        file_get_contents(
            "https://api.telegram.org/bot1797199414:AAHScM49DEZDvn8SjsoYANn2flW090oXWhE/sendMessage?chat_id=-1001327632869&text=" . json_encode(
                $content
            )
        );

        if (!Signature::check(
            json_encode($content['data'], JSON_UNESCAPED_SLASHES),
            $content['signature'],
            $_ENV['PROJECT_SECRET']
        )) {
            throw new Exception('Forbidden', 403);
        }

        try {
            $status = $content['data']['status'];
            $amount = $content['data']['processedAmount'];

            if (in_array(
                $requisition->getStatus(),
                [
                    Requisition::PAID,
                    Requisition::PAID_OUT,
                    Requisition::REJECT
                ]
            )) {
                return new JsonResponse();
            }

            if ($status == 'FAIL') {
                $status = 'REJECT';
            }

            if ($status == 'PROCESSED') {
                $status = 'PAID';

                switch ($requisition->getExchangePair()->getPayment()->getExchangeObject()->getType()) {
                    case Currency::TYPE:
                        {
                            $amount = floor($amount * 100) / 100;

                            $requisition->setTmpCount($amount);
                        }
                        break;
                    case Cryptocurrency::TYPE:
                        {
                            $requisition->setPayment(json_decode($requisition->getInfo(), true)['amount']);

                            $requisition->setPayout($requisition->getPayment() / $requisition->getCourse());
                        }
                        break;
                }
            }

            $requisition->setStatus($status);

            $this->entityManager->flush();

//            Payout::init($requisition, $this->entityManager, $this->apiClient);

        } catch (Exception $exception) {
            file_get_contents(
                "https://api.telegram.org/bot1132760191:AAHk2HgyE-aJxaTvXzGASSDNPzFkTGoGYYo/sendMessage?chat_id=-1001224527995&parse_mode=html&text=" . $exception->getMessage()
            );
        }

        return new Response();
    }
}