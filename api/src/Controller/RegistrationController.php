<?php

namespace App\Controller;

use App\Entity\Client;
use App\Service\FieldsChecker;
use App\Service\JWT;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegistrationController
 * @package App\Controller
 */
class RegistrationController extends AbstractController
{

    protected const needleUserFields = [
        'username',
        'password',
        'firstname',
        'lastname'
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    protected UserPasswordEncoderInterface $encoder;

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var JWT
     */
    protected JWT $JWT;

    /**
     * RegistrationController constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface $entityManager
     * @param JWT $JWT
     */
    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EntityManagerInterface $entityManager,
        JWT $JWT
    ) {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
        $this->JWT = $JWT;
    }

    /**
     * @Route("/api/registration", name="registration")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return mixed
     * @throws Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $check = FieldsChecker::check($data, self::needleUserFields);

        if (!$check) {
            throw new Exception('Invalid fields', Response::HTTP_CONFLICT);
        }

        $client = $this->entityManager->getRepository(Client::class)->findOneBy(['email' => $data['username']]);

        if ($client) {
            throw new BadRequestException('This user already exist', Response::HTTP_BAD_REQUEST);
        }

        try {
            $client = new Client();
            $client->setPassword($encoder->encodePassword($client, $data['password']));
            $client->setEmail($data['username']);
            $client->setFirstname($data['firstname']);
            $client->setLastname($data['lastname']);

            $this->entityManager->persist($client);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new Exception($exception->getMessage(), Response::HTTP_CONFLICT);
        }

        return new JsonResponse(['token' => $this->JWT->createUserAuthToken($client)]);
    }
}
