<?php


namespace App\Controller\Requisition;


use App\Entity\Client;
use App\Entity\Requisition;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequisitionCollectionAction
 * @package App\Controller\Requisition
 */
class RequisitionCollectionAction extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * RequisitionCollectionAction constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $user = $this->getUser();
        $params = $request->query->all();

        if ($user instanceof Client) {
            return $this->entityManager->getRepository(Requisition::class)->getRequisitions($user, $params);
        }

        return $this->entityManager->getRepository(Requisition::class)->getRequisitions(null, $params);
    }
}
