<?php


namespace App\Controller;

use App\Entity\Cryptocurrency;
use App\Entity\Currency;
use App\Entity\Requisition;
use App\Service\Signature;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PayoutCallbackController
 * @package App\Controller
 */
class PayoutCallbackController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * PaymentCallbackController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/payout-status-check/{id}", name="payout_status_check")
     * @param Request $request
     * @param Requisition $requisition
     * @return Response
     * @throws Exception
     */
    public function payoutStatusCheck(Request $request, Requisition $requisition)
    {
        $response = json_decode($request->getContent(), true);

        if (!Signature::check(
            json_encode($response['data'], JSON_UNESCAPED_SLASHES),
            $response['signature'],
            $_ENV['PROJECT_SECRET']
        )) {
            throw new Exception('Forbidden', 403);
        }

        file_get_contents(
            "https://api.telegram.org/bot1797199414:AAHScM49DEZDvn8SjsoYANn2flW090oXWhE/sendMessage?chat_id=-1001327632869&text=" . json_encode(
                $response
            )
        );

        $paidAmount = $response['data']['processedAmount'];
        $status = $response['data']['status'];

        if ($requisition->getStatus() == Requisition::PAID_OUT) {
            return new JsonResponse();
        }

        if ($status == 'PROCESSED') {
            $requisition->setStatus(Requisition::PAID_OUT);

            switch ($requisition->getExchangePair()->getPayment()->getExchangeObject()->getType()) {
                case Currency::TYPE:
                    {
                        $requisition->setPayout($paidAmount);

                        $requisition->setCourse($requisition->getTmpCount() / $paidAmount);
                    }
                    break;
                case Cryptocurrency::TYPE:
                    {
                        $requisition->setPayout($paidAmount);

                        $requisition->setCourse($requisition->getPayout() / $requisition->getPayment());
                    }
                    break;
            }
        }

        if ($status == 'FAIL' || $status == 'CANCEL') {
            $requisition->setStatus(Requisition::ERROR);
        }

        $this->entityManager->flush();

        return new JsonResponse();
    }
}