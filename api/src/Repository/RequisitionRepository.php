<?php

namespace App\Repository;

use App\Entity\Client;
use App\Entity\Requisition;
use App\Service\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Requisition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Requisition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Requisition[]    findAll()
 * @method Requisition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequisitionRepository extends ServiceEntityRepository
{
    const PER_ITEMS = 12;

    /**
     * RequisitionRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Requisition::class);
    }

    /**
     * @param Client|null $client
     * @param null $params
     * @return int|mixed|string
     */
    public function getRequisitions(Client $client = null, $params = null)
    {
        $firstResult = (($params['page'] ?? 1) - 1) * self::PER_ITEMS;

        $queryBuilder = $this->createQueryBuilder('requisition')->orderBy('requisition.id','DESC');

        if ($client) {
            $queryBuilder
                ->andWhere('requisition.client = :client')
                ->setParameter('client', $client);
        }

        return Paginator::getInstance()->getDoctrinePaginator($queryBuilder, $firstResult, self::PER_ITEMS);
    }
}
