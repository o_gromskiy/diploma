<?php

namespace App\Repository;

use App\Entity\ExchangePart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExchangePart|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangePart|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangePart[]    findAll()
 * @method ExchangePart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangePartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExchangePart::class);
    }

    // /**
    //  * @return ExchangePart[] Returns an array of ExchangePart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExchangePart
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
