<?php

namespace App\Repository\Rates;

use App\Entity\Rates\WhiteBitRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WhiteBitRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method WhiteBitRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method WhiteBitRates[]    findAll()
 * @method WhiteBitRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WhiteBitRatesRepository extends ServiceEntityRepository
{

    /**
     * WhiteBitRatesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WhiteBitRates::class);
    }

    /**
     * @param string $asset
     * @return int|mixed|string
     */
    public function getRatesByAsset(string $asset)
    {
        return $this->createQueryBuilder('rates')
            ->orderBy('rates.id', 'DESC')
            ->andWhere('rates.asset = :asset')
            ->setParameter('asset', $asset)
            ->getQuery()
            ->getResult();
    }
}
