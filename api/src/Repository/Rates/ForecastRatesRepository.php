<?php

namespace App\Repository\Rates;

use App\Entity\Rates\ForecastRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ForecastRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForecastRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForecastRates[]    findAll()
 * @method ForecastRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForecastRatesRepository extends ServiceEntityRepository
{

    /**
     * ForecastRatesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForecastRates::class);
    }

    /**
     * @param string $asset
     * @return int|mixed|string
     */
    public function getRatesByAsset(string $asset)
    {
        return $this->createQueryBuilder('rates')
            ->orderBy('rates.id', 'DESC')
            ->andWhere('rates.asset = :asset')
            ->setParameter('asset', $asset)
            ->getQuery()
            ->getResult();
    }
}
