<?php

namespace App\Repository\Rates;

use App\Entity\Rates\HuobiRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HuobiRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method HuobiRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method HuobiRates[]    findAll()
 * @method HuobiRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HuobiRatesRepository extends ServiceEntityRepository
{

    /**
     * HuobiRatesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HuobiRates::class);
    }

    /**
     * @param string $asset
     * @return int|mixed|string
     */
    public function getRatesByAsset(string $asset)
    {
        return $this->createQueryBuilder('rates')
            ->orderBy('rates.id', 'DESC')
            ->andWhere('rates.asset = :asset')
            ->setParameter('asset', $asset)
            ->getQuery()
            ->getResult();
    }
}
