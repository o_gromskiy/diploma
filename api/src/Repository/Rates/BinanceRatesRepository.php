<?php

namespace App\Repository\Rates;

use App\Entity\Rates\BinanceRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BinanceRates|null find($id, $lockMode = null, $lockVersion = null)
 * @method BinanceRates|null findOneBy(array $criteria, array $orderBy = null)
 * @method BinanceRates[]    findAll()
 * @method BinanceRates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BinanceRatesRepository extends ServiceEntityRepository
{

    /**
     * BinanceRatesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BinanceRates::class);
    }

    /**
     * @param string $asset
     * @return int|mixed|string
     */
    public function getRatesByAsset(string $asset)
    {
        return $this->createQueryBuilder('rates')
            ->orderBy('rates.id', 'DESC')
            ->andWhere('rates.asset = :asset')
            ->setParameter('asset', $asset)
            ->getQuery()
            ->getResult();
    }
}
