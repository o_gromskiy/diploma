<?php

namespace App\Service\Rates;

use App\Entity\Cryptocurrency;
use App\Entity\Rates\BinanceRates;
use App\Entity\Rates\ForecastRates;
use App\Entity\Rates\HuobiRates;
use App\Entity\Rates\WhiteBitRates;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ForecastRateService
{

    /**
     * @param EntityManagerInterface $entityManager
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public static function updateRate(EntityManagerInterface $entityManager)
    {
        $cryptocurrencies = $entityManager->getRepository(Cryptocurrency::class)->findAll();

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        foreach ($cryptocurrencies as $cryptocurrency) {

            $binanceRates = $entityManager->getRepository(BinanceRates::class)->getRatesByAsset($cryptocurrency->getAsset());
            $huobiRates = $entityManager->getRepository(HuobiRates::class)->getRatesByAsset($cryptocurrency->getAsset());
            $whiteBitRates = $entityManager->getRepository(WhiteBitRates::class)->getRatesByAsset($cryptocurrency->getAsset());

            $forecastRates = $entityManager->getRepository(ForecastRates::class)->getRatesByAsset($cryptocurrency->getAsset());

            $countOfForecastRates = count($forecastRates);

            if ($countOfForecastRates > 0) {
                $entityManager->remove($forecastRates[$countOfForecastRates - 1]);

                $tmpRates = [$binanceRates[count($binanceRates) - 1]->getRate(), $huobiRates[count($huobiRates) - 1]->getRate(), $whiteBitRates[count($whiteBitRates) - 1]->getRate()];

                self::createRates($cryptocurrency->getAsset(), self::calculateMedian($tmpRates), $binanceRates[count($binanceRates) - 1]->getTime(), $entityManager);

            } else {
                for ($i = 0; $i < count($binanceRates); $i++) {

                    $tmpRates = [$binanceRates[$i]->getRate(), $huobiRates[$i]->getRate(), $whiteBitRates[$i]->getRate()];

                    self::createRates($cryptocurrency->getAsset(), self::calculateMedian($tmpRates), $binanceRates[count($binanceRates) - 1]->getTime(), $entityManager);
                }
            }
        }

        $entityManager->flush();

    }

    /**
     * @return void
     */
    private static function createRates(string $asset, string $rate, string $time, EntityManagerInterface $entityManager)
    {
        $forecastRates = new ForecastRates();

        $forecastRates->setAsset($asset);
        $forecastRates->setRate($rate);
        $forecastRates->setTime($time);

        $entityManager->persist($forecastRates);
    }

    /**
     * @param $arr
     * @return float|int|mixed
     */
    private static function calculateMedian($arr)
    {
        //total numbers in array
        $count = count($arr);

        // find the middle value, or the lowest middle value
        $middleval = floor(($count - 1) / 2);

        // odd number, middle is the median
        if ($count % 2) {
            $median = $arr[$middleval];

            // even number, calculate avg of 2 medians
        } else {
            $low = $arr[$middleval];
            $high = $arr[$middleval + 1];
            $median = (($low + $high) / 2);
        }

        return $median;
    }

}