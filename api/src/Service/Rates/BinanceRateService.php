<?php

namespace App\Service\Rates;

use App\Entity\Cryptocurrency;
use App\Entity\Rates\BinanceRates;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\NativeHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BinanceRateService
{

    public const CLOSE_PRICE_INDEX = 4;

    public const TIME_INDEX = 0;

    /**
     * @param EntityManagerInterface $entityManager
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public static function updateRate(EntityManagerInterface $entityManager)
    {
        $client = new NativeHttpClient();

        $cryptocurrencies = $entityManager->getRepository(Cryptocurrency::class)->findAll();

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        foreach ($cryptocurrencies as $cryptocurrency) {

            $binanceRates = $entityManager->getRepository(BinanceRates::class)->getRatesByAsset($cryptocurrency->getAsset());

            $countOfBinanceRates = count($binanceRates);

            if ($countOfBinanceRates > 0) {
                $interval = 1;

                $entityManager->remove($binanceRates[$countOfBinanceRates - 1]);

            } else {
                $interval = 100;
            }

            $response = $client->request("GET", "https://api.binance.com/api/v3/klines?symbol=" . $cryptocurrency->getAsset() . "USDT&interval=1h&limit=" . $interval);

            $marketData = $response->toArray();

            foreach ($marketData as $data) {

                if (!isset($data[self::CLOSE_PRICE_INDEX])) {
                    continue;
                }

                self::createRates($cryptocurrency->getAsset(), $data[self::CLOSE_PRICE_INDEX], $data[self::TIME_INDEX] / 1000, $entityManager);
            }
        }

        $entityManager->flush();

    }

    /**
     * @return void
     */
    private static function createRates(string $asset, string $rate, string $time, EntityManagerInterface $entityManager)
    {
        $binanceRates = new BinanceRates();

        $binanceRates->setAsset($asset);
        $binanceRates->setRate($rate);
        $binanceRates->setTime($time);

        $entityManager->persist($binanceRates);
    }

}