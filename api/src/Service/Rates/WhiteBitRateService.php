<?php

namespace App\Service\Rates;

use App\Entity\Cryptocurrency;
use App\Entity\Rates\WhiteBitRates;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\NativeHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class WhiteBitRateService
{

    public const CLOSE_PRICE_INDEX = 2;

    public const TIME_INDEX = 0;

    /**
     * @param EntityManagerInterface $entityManager
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public static function updateRate(EntityManagerInterface $entityManager)
    {
        $client = new NativeHttpClient();

        $cryptocurrencies = $entityManager->getRepository(Cryptocurrency::class)->findAll();

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        foreach ($cryptocurrencies as $cryptocurrency) {

            $whiteBitRates = $entityManager->getRepository(WhiteBitRates::class)->getRatesByAsset($cryptocurrency->getAsset());

            $countOfWhiteBitRates = count($whiteBitRates);

            if ($countOfWhiteBitRates > 0) {
                $interval = 1;

                $entityManager->remove($whiteBitRates[$countOfWhiteBitRates - 1]);

            } else {
                $interval = 100;
            }

            $response = $client->request("GET", "https://whitebit.com/api/v1/public/kline?market=" . $cryptocurrency->getAsset() . "_USDT&interval=1h&limit=" . $interval);

            $response = $response->toArray();

            $marketData = $response['result'] ?? null;

            if (!$marketData) {
                continue;
            }

            foreach ($marketData as $data) {

                if (!isset($data[self::CLOSE_PRICE_INDEX])) {
                    continue;
                }

                self::createRates($cryptocurrency->getAsset(), $data[self::CLOSE_PRICE_INDEX], $data[self::TIME_INDEX], $entityManager);
            }
        }

        $entityManager->flush();
    }

    /**
     * @return void
     */
    private static function createRates(string $asset, string $rate, string $time, EntityManagerInterface $entityManager)
    {
        $whiteBitRates = new WhiteBitRates();

        $whiteBitRates->setAsset($asset);
        $whiteBitRates->setRate($rate);
        $whiteBitRates->setTime($time);

        $entityManager->persist($whiteBitRates);
    }

}