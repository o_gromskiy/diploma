<?php


namespace App\Service;

/**
 * Class FieldsChecker
 * @package App\Service
 */
class FieldsChecker
{
    /**
     * @param array $attributes
     * @param array $needle
     * @return bool
     */
    public static function check(array $attributes, array $needle): bool
    {
        foreach ($needle as $value) {
            if (!array_key_exists($value, $attributes)) {
                return false;
            }
        }

        return true;
    }
}
