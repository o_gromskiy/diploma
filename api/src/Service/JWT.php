<?php


namespace App\Service;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * Class JWT
 * @package App\Service
 */
class JWT
{

    /**
     * @var JWTTokenManagerInterface
     */
    protected JWTTokenManagerInterface $JWTManager;

    /**
     * JWT constructor.
     * @param JWTTokenManagerInterface $JWTManager
     */
    public function __construct(JWTTokenManagerInterface $JWTManager)
    {
        $this->JWTManager = $JWTManager;
    }

    /**
     * @param User $user
     * @return string
     */
    public function createUserAuthToken(User $user): string
    {
        return $this->JWTManager->create($user);
    }
}
